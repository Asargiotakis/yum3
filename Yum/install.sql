DROP SCHEMA if EXISTS `yum`;

CREATE SCHEMA IF NOT EXISTS `yum` DEFAULT CHARACTER SET utf8 ;

USE yum;

CREATE TABLE user (
    id BIGINT NOT NULL AUTO_INCREMENT,
    email VARCHAR(250) NOT NULL,
    first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100) NOT NULL,
    role ENUM('chef', 'hungry', 'admin'),
    password VARCHAR(61) NOT NULL,
    registration_date DATE NOT NULL,
    picture MEDIUMBLOB,
    approved BOOLEAN,
    secret VARCHAR(250),
    secret_creation TIMESTAMP,
    version INT NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (email)
);

CREATE TABLE food (
    id BIGINT NOT NULL AUTO_INCREMENT,
    food_type ENUM('main dish', 'salad', 'soft drink'),
    food_name VARCHAR(100) NOT NULL,
    description VARCHAR(255),
    price DECIMAL(4 , 2 ) NOT NULL,
    archived BOOLEAN DEFAULT 0,
    version INT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE daily_menu (
    id BIGINT NOT NULL AUTO_INCREMENT,
    of_date DATE NOT NULL,
    expired BOOLEAN DEFAULT 0,
    version INT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE daily_menu_food (
    daily_menu_id BIGINT NOT NULL,
    food_id BIGINT NOT NULL,
    PRIMARY KEY (daily_menu_id , food_id),
    FOREIGN KEY (daily_menu_id)
        REFERENCES daily_menu (id),
    FOREIGN KEY (food_id)
        REFERENCES food (id)
);

CREATE TABLE daily_order (
    id BIGINT NOT NULL AUTO_INCREMENT,
    user_id BIGINT NOT NULL,
    daily_menu_id BIGINT NOT NULL,
    final BOOLEAN DEFAULT 0,
    version INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id)
        REFERENCES user (id),
    FOREIGN KEY (daily_menu_id)
        REFERENCES daily_menu (id)
);

CREATE TABLE order_item (
    daily_order_id BIGINT NOT NULL,
    food_id BIGINT NOT NULL,
    quantity INT NOT NULL,
    PRIMARY KEY (daily_order_id , food_id),
    FOREIGN KEY (daily_order_id)
        REFERENCES daily_order (id),
    FOREIGN KEY (food_id)
        REFERENCES food (id)
);

CREATE TABLE Yum (
    id BIGINT NOT NULL,
    terms MEDIUMTEXT,
    privacy MEDIUMTEXT,
    notes MEDIUMTEXT,
    deadline TIME,
    currency VARCHAR(10),
    version INT NOT NULL,
    foods_version INT NOT NULL,
    PRIMARY KEY (id)
);
/* Insert Admin */
insert into user (id, email, first_name,last_name, role, password, registration_date,  approved, version) values 
(1,'admin@yum.com', 'admin', 'admin', 'admin', '$2a$10$xcQ2xnAElA4omtDNJf8vNu1sh62gkR6D0dx.Sg22PoXuDVzuVnOhu', CURDATE(), true, 0);

/* Insert Yum data */
INSERT INTO yum (id, terms, privacy, notes, deadline, currency, version, foods_version) 
	VALUES ( 1,
    '<h3>Sample Terms of Service</h3>&#10;
    <p>&#160;</p>&#10;
    <p>Last updated: <strong>24th of May 2017</strong></p>&#10;
    <p>&#160;</p>&#10;
    <p>Please read these Terms of Service (&#34;Terms&#34;, &#34;Terms of Service&#34;) carefully before using the Yum Meal Manager website.</p>&#10;
    <p>Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.</p>&#10;
    <p><strong>By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.</strong></p>&#10;<p>&#160;</p>&#10;
    <p><strong>Termination</strong></p>&#10;
    <p>We may terminate or suspend access to our Service immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.</p>&#10;
    <p>All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.</p>&#10;
    <p>&#160;</p>&#10;
    <p><strong>Content</strong></p>&#10;
    <p>Our Service allows you to post, link, store, share and otherwise make available certain information, text, graphics, videos, or other material (&#34;Content&#34;). You are responsible for the &#8230;</p>&#10;
    <p>&#160;</p>&#10;
    <p><strong>Links To Other Web Sites</strong></p>&#10;
    <p>Our Service may contain links to third-party web sites or services that are not owned or controlled by Yum Meal Manager.</p>&#10;
    <p>Yum Meal Manager has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party web sites or services. You further acknowledge and agree that Yum Meal Manager shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such web sites or services.</p>&#10;
    <p>&#160;</p>&#10;
    <p><strong>Changes</strong></p>&#10;
    <p>We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will try to provide at least 7 days notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.</p>&#10;
    <p>&#160;</p>&#10;
    <p><strong>Contact Us</strong></p>&#10;
    <p>If you have any questions about these Terms, please contact us.</p>',
    
    '<h3>Sample&#160;Privacy policy</h3>&#10;
    <p>&#160;</p>&#10;
    <p>Last updated: <strong>24th of May 2017</strong></p>&#10;
    <p>&#160;</p>&#10;
    <p>Yum Meal Manager (&#34;us&#34;, &#34;we&#34;, or &#34;our&#34;) website (the &#34;Site&#34;). This page informs you of our policies regarding the collection, use and disclosure of Personal Information we receive from users of the Site.</p>&#10;
    <p>We use your Personal Information only for providing and improving the Site. By using the Site, you agree to the collection and use of information in accordance with this policy.</p>&#10;
    <p>&#160;</p>&#10;
    <p><strong>Information Collection And Use</strong></p>&#10;
    <p>While using our Site, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you. Personally identifiable information may include, but is not limited to your name (&#34;Personal Information&#34;).</p>&#10;
    <p>&#160;</p>&#10;
    <p><strong>Log Data</strong></p>&#10;
    <p>Like many site operators, we collect information that your browser sends whenever you visit our Site (&#34;Log Data&#34;).</p>&#10;
    <p>This Log Data may include information such as your computer Internet Protocol (&#34;IP&#34;) address, browser type, browser version, the pages of our Site that you visit, the time and date of your visit, the time spent on those pages and other statistics.</p>&#10;
    <p>In addition, we may use third party services such as Google Analytics that collect, monitor and analyze this &#8230;</p>&#10;
    <p>&#160;</p>&#10;
    <p><strong>Communications</strong></p>&#10;
    <p>We may use your Personal Information to contact you with newsletters, marketing or promotional materials and other information that ...</p>&#10;
    <p>&#160;</p>&#10;
    <p><strong>Cookies</strong></p>&#10;
    <p>Cookies are files with small amount of data, which may include an anonymous unique identifier. Cookies are sent to your browser from a web site and stored on your computers hard drive.</p>&#10;
    <p>Like many sites, we use &#34;cookies&#34; to collect information. You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Site.</p>&#10;
    <p>&#160;</p>&#10;
    <p><strong>Security</strong></p>&#10;
    <p>The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage, is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security.</p>&#10;
    <p>&#160;</p>&#10;
    <p><strong>Changes To This Privacy Policy</strong></p>&#10;
    <p>This Privacy Policy is effective as of 24th of May 2017 and will remain in effect except with respect to any changes in its provisions in the future, which will be in effect immediately after being posted on this page.</p>&#10;
    <p>We reserve the right to update or change our Privacy Policy at any time and you should check this Privacy Policy periodically. Your continued use of the Service after we post any modifications to the Privacy Policy on this page will constitute your acknowledgment of the modifications and your consent to abide and be bound by the modified Privacy Policy.</p>&#10;
    <p>If we make any material changes to this Privacy Policy, we will notify you either through the email address you have provided us, or by placing a prominent notice on our website.</p>&#10;
    <p>&#160;</p>&#10;
    <p><strong>Contact Us</strong></p>&#10;
    <p>If you have any questions about this Privacy Policy, please contact us.</p>',
    
    '<p>- All meals <strong>include</strong> slices of bread.</p>&#10;
    <p>- Meals are delivered in a micorwave compatible plastic box. Disposable utensils are also included.</p>&#10;
    <p>- Delivery time is at <strong>13:30</strong>.</p>&#10;
    <p>- Payments will be collected by the reception desk. Mind to have the exact amount.</p>',
    '12:00',
    '&euro;',
    0,
    0);
