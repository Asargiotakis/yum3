/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.data.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class OrderItemKey implements Serializable {

    @Column(name = "daily_order_id", nullable = false)
    protected Long dailyOrderId;

    @Column(name = "food_id", nullable = false)
    protected Long foodId;

    public OrderItemKey() {
    }

    public OrderItemKey(Long dailyOrderId, Long foodId) {
        this.dailyOrderId = dailyOrderId;
        this.foodId = foodId;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }

    public void setDailyOrderId(Long dailyOrderId) {
        this.dailyOrderId = dailyOrderId;
    }

    public void setFoodId(Long foodId) {
        this.foodId = foodId;
    }

    @Override
    public String toString() {
        return "OrderItemKey{" + "dailyOrderId=" + dailyOrderId + ", foodId=" + foodId + '}';
    }

}
