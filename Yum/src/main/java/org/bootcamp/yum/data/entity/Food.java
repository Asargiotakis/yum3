/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.data.entity;

import java.util.List;
import java.util.Objects;
import java.util.ArrayList;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Version;

import org.bootcamp.converter.FoodConverter;
import org.bootcamp.enums.FoodEnum;
import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.api.model.chef.FoodDetails;

@Entity
@Table(name = "food")
public class Food implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "food_type")
    @Convert(converter = FoodConverter.class)
    private FoodEnum type;

    @Column(name = "food_name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "archived")
    private boolean archived;

    @Version
    @Column(name = "version")
    private int version;

    public Food() {
    }

    public Food(long id) {
        this.id = id;
    }

//     //multiplicity relationship with OrderItem
//    @OneToMany(mappedBy="food")
//    private List<OrderItem> orderItems;
//    
//    //method related to multiplicity with OrderItem
//    public List<OrderItem> getOrderItems() {
//        return orderItems;
//    }
//
//    //method related to multiplicity with OrderItem
//    public void addDailyOrder(OrderItem orderItem) {
//        this.orderItems.add(orderItem);
//    }
    //multiplicity relationship with DailyMenu
    @ManyToMany
    @JoinTable(
            name = "daily_menu_food",
            joinColumns = @JoinColumn(name = "food_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "daily_menu_id", referencedColumnName = "id"))
    private List<DailyMenu> dailyMenus;

    //method related to multiplicity with DailyMenu
    public List<DailyMenu> getDailyMenus() {
        return dailyMenus;
    }

    //method related to multiplicity with DailyMenu
    public void addDailyMenu(DailyMenu dailyMenu) {
        if (this.dailyMenus == null) {
            dailyMenus = new ArrayList<>();
        }
        this.dailyMenus.add(dailyMenu);
    }

    public void removeDailyMenu(DailyMenu dailyMenu) {
        this.dailyMenus.remove(dailyMenu);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FoodEnum getType() {
        return type;
    }

    public void setType(FoodEnum type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public long getId() {
        return id;
    }

    public void removeFromFutureDailyMenus(DailyMenu dailyMenu) {
        dailyMenus.remove(dailyMenu);
    }

    public boolean updateFields(FoodDetails foodDetails, boolean canEdit) throws ApiException {

        //if the food can be fully modified.
        //update the none empty/null fields
        if (canEdit) {

            FoodEnum type;
            try {
                type = FoodEnum.valueOf(foodDetails.getFoodType().trim().toUpperCase());
            } catch (IllegalArgumentException e) {
                throw new ApiException(400, "Bad request, invalid food type");
            }

            //check if no changes send
            if (this.type.equals(type) && this.name.equals(foodDetails.getName().trim())
                    && (this.description != null && this.description.equals(foodDetails.getDescription().trim()))
                    && this.price.equals(foodDetails.getPrice())) {

                return false;
            }
            setType(type);
            setName(foodDetails.getName().trim().isEmpty() ? this.name : foodDetails.getName().trim());
            setDescription(foodDetails.getDescription().trim().isEmpty() ? this.description : foodDetails.getDescription().trim());

            if (foodDetails.getPrice() != null
                    && (foodDetails.getPrice().doubleValue() > 0 && foodDetails.getPrice().doubleValue() < 100)) {
                setPrice(foodDetails.getPrice());
            }
        } //if the food has been ordered
        else {
            if (this.description.equals(foodDetails.getDescription().trim())) {
                return false;
            }
            setDescription(foodDetails.getDescription().trim().isEmpty() ? this.description : foodDetails.getDescription().trim());
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 47 * hash + Objects.hashCode(this.type);
        hash = 47 * hash + Objects.hashCode(this.name);
        hash = 47 * hash + Objects.hashCode(this.description);
        hash = 47 * hash + Objects.hashCode(this.price);
        hash = 47 * hash + (this.archived ? 1 : 0);
        hash = 47 * hash + this.version;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        return obj.hashCode() == hashCode();
    }

    @Override
    public String toString() {
        return "Food{"
                + "id= " + id + "\n type= " + type
                + "\n name= " + name + "\n description= " + description
                + "\n price= " + price + "\n archived= " + archived
                + "\n version= " + version + '}' + "\n";
    }

}
