/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.data.repository;

import org.joda.time.LocalDate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import org.bootcamp.enums.FoodEnum;
import org.bootcamp.yum.data.entity.Food;

/**
 *
 * @author admin
 */
@Repository
public interface FoodRepository extends PagingAndSortingRepository<Food, Long> {

    Food findById(long id);

    Food findByName(String name);

    Food findByIdAndDailyMenusOfDateGreaterThanEqual(long id, LocalDate date);

    Food findByIdAndDailyMenusId(Long id, Long menuId);

    Page<Food> findAll(Pageable pageable);

    Page<Food> findByArchived(boolean archived, Pageable pageable);

    Page<Food> findByArchivedAndType(boolean archived, FoodEnum type, Pageable pageable);
}
