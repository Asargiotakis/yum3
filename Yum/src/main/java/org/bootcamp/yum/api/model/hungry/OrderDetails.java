/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api.model.hungry;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.constraints.*;
import javax.validation.Valid;

import org.joda.time.LocalDate;

import io.swagger.annotations.ApiModelProperty;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.bootcamp.yum.api.model.OrderItem;
import org.bootcamp.validator.CheckDublicateIds;

/**
 * OrderDetails
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-15T10:41:58.894+03:00")

public class OrderDetails {

    @NotNull
    @JsonProperty("date")
    private LocalDate date = null;
    @NotNull
    @Min(0)
    @JsonProperty("menuVersion")
    private Integer menuVersion = null;
    @NotNull
    @Min(1)
    @JsonProperty("menuId")
    private Long menuId = null;
    @NotNull
    @JsonProperty("sendMail")
    private Boolean sendMail = null;
    @NotNull
    @Size(min = 1)
    //Custom constraint
    @CheckDublicateIds(message = "Order items cannot contain dublicate food ids")
    @Valid
    @JsonProperty("orderItems")
    private List<OrderItem> orderItems = new ArrayList<OrderItem>();

    public OrderDetails date(LocalDate date) {
        this.date = date;
        return this;
    }

    /**
     * Get date
     *
     * @return date
     *
     */
    @ApiModelProperty(value = "")
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public OrderDetails menuVersion(Integer menuVersion) {
        this.menuVersion = menuVersion;
        return this;
    }

    /**
     * Get menuVersion
     *
     * @return menuVersion
     *
     */
    @ApiModelProperty(value = "")
    public Integer getMenuVersion() {
        return menuVersion;
    }

    public void setMenuVersion(Integer menuVersion) {
        this.menuVersion = menuVersion;
    }

    public OrderDetails menuId(Long menuId) {
        this.menuId = menuId;
        return this;
    }

    /**
     * Get menuId
     *
     * @return menuId
     *
     */
    @ApiModelProperty(value = "")
    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public OrderDetails sendMail(Boolean sendMail) {
        this.sendMail = sendMail;
        return this;
    }

    /**
     * Get sendMail
     *
     * @return sendMail
     *
     */
    @ApiModelProperty(value = "")
    public Boolean getSendMail() {
        return sendMail;
    }

    public void setSendMail(Boolean sendMail) {
        this.sendMail = sendMail;
    }

    public OrderDetails orderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
        return this;
    }

    public OrderDetails addOrderItemsItem(OrderItem orderItemsItem) {
        this.orderItems.add(orderItemsItem);
        return this;
    }

    /**
     * Get orderItems
     *
     * @return orderItems
     *
     */
    @ApiModelProperty(value = "")
    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderDetails orderDetails = (OrderDetails) o;
        return Objects.equals(this.date, orderDetails.date)
                && Objects.equals(this.menuVersion, orderDetails.menuVersion)
                && Objects.equals(this.menuId, orderDetails.menuId)
                && Objects.equals(this.sendMail, orderDetails.sendMail)
                && Objects.equals(this.orderItems, orderDetails.orderItems);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, menuVersion, menuId, sendMail, orderItems);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class OrderDetails {\n");

        sb.append("    date: ").append(toIndentedString(date)).append("\n");
        sb.append("    menuVersion: ").append(toIndentedString(menuVersion)).append("\n");
        sb.append("    menuId: ").append(toIndentedString(menuId)).append("\n");
        sb.append("    sendMail: ").append(toIndentedString(sendMail)).append("\n");
        sb.append("    orderItems: ").append(toIndentedString(orderItems)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
