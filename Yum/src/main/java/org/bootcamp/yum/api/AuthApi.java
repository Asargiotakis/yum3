/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import io.swagger.annotations.*;

import org.bootcamp.yum.api.model.AccountDetails;
import org.bootcamp.yum.api.model.auth.ChangePassword;
import org.bootcamp.yum.api.model.auth.Email;
import org.bootcamp.yum.api.model.auth.Login;
import org.bootcamp.yum.api.model.auth.LoggedUser;
import org.bootcamp.yum.api.model.auth.TermsAndPolicy;
import org.bootcamp.yum.exception.ApiException;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-27T16:35:28.866+03:00")

@Api(value = "auth", description = "the auth API")
@RequestMapping(value = "/api")
public interface AuthApi {

    @CrossOrigin
    @ApiOperation(value = "", notes = "Allow users to change their password", response = Void.class, tags = {"auth",})
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Password Changed", response = Void.class)
        ,
        @ApiResponse(code = 400, message = "If the given password is invalid or the secret is invalid/expired", response = Void.class)})
    @RequestMapping(value = "/auth/changepwd",
            produces = {"application/json"},
            method = RequestMethod.PUT)
    ResponseEntity<Void> authChangepwdPut(@ApiParam(value = "Contains the secret and the new password", required = true) @Valid @RequestBody ChangePassword changePassword, Errors errors) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "", notes = "Allow users to reset their password", response = Void.class, tags = {"auth",})
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Email address found and reset link sent to the user's email", response = Void.class)
        ,
        @ApiResponse(code = 400, message = "If the given email format is invalid", response = Void.class)
        ,
        @ApiResponse(code = 404, message = "If the given email doesn't exists in the database or has invalid format", response = Void.class)})
    @RequestMapping(value = "/auth/forgotpwd",
            produces = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<Void> authForgotpwdPost(@ApiParam(value = "The email to reset password", required = true) @Valid @RequestBody Email email, Errors errors) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "", notes = "Allow users to log in, and to receive a Token", response = LoggedUser.class, tags = {"auth",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Login Success", response = LoggedUser.class)
        ,
        @ApiResponse(code = 403, message = "If user is not found (bad credentials) OR if user can not login (not approved)", response = LoggedUser.class)})
    @RequestMapping(value = "/auth/login",
            produces = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<LoggedUser> authLoginPost(@ApiParam(value = "The email/password", required = true) @Valid @RequestBody Login credentials, Errors errors) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "", notes = "Allow users to register", response = Void.class, tags = {"auth",})
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Registration Success", response = Void.class)
        ,
        @ApiResponse(code = 400, message = "If the given email exists in the database or has invalid format or password length is less than six characters", response = Void.class)})
    @RequestMapping(value = "/auth/register",
            produces = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<Void> authRegisterPost(@ApiParam(value = "The account details", required = true) @Valid @RequestBody AccountDetails credentials, Errors errors) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "", notes = "Retrieve terms of service and privacy policy", response = TermsAndPolicy.class, tags = {"auth",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Terms of service and Privacy policy", response = TermsAndPolicy.class)})
    @RequestMapping(value = "/auth/terms",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<TermsAndPolicy> authTermsGet();
}
