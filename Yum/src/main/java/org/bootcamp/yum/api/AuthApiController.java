/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;

import javax.validation.Valid;
import javax.persistence.OptimisticLockException;

import io.swagger.annotations.*;

import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;

import org.bootcamp.yum.api.model.AccountDetails;
import org.bootcamp.yum.api.model.auth.ChangePassword;
import org.bootcamp.yum.api.model.auth.Email;
import org.bootcamp.yum.api.model.auth.Login;
import org.bootcamp.yum.api.model.auth.LoggedUser;
import org.bootcamp.yum.api.model.auth.TermsAndPolicy;
import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.service.AuthService;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-19T18:39:30.829+03:00")

@Controller
public class AuthApiController implements AuthApi {

    private AuthService authService;

    @Autowired
    public AuthApiController(AuthService authService) {
        this.authService = authService;
    }

    /**
     * send a reset password request to the user with the given e-mail
     *
     * @param email user's e-mail
     * @param errors validation errors from request body params.
     * @return void
     * @throws ApiException throws api exception if the e-mail format is invalid
     * or if there is not user with the given e-mail in the db.
     */
    @Override
    public ResponseEntity<Void> authForgotpwdPost(@ApiParam(value = "The email to reset password", required = true) @Valid @RequestBody Email email, Errors errors) throws ApiException {

        //check if we have validation errors in credentials
        if (errors.hasErrors()) {
            throw new ApiException(errors.getFieldError().getField(), 400);
        }

        authService.authForgotpwdPost(email);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * Checks user's credentials and return the login token.
     *
     * @param credentials contains the user login credentials.
     * @param errors a list that contains all the validation errors that
     * occurred threw the e-mail and password validations.
     * @return returns an HTTP status 200 (OK) with the login token if the user
     * authenticate successfully else returns an HTTP status 403 (FORBIDDEN).
     * @throws org.bootcamp.yum.api.ApiException
     */
    @Override
    public ResponseEntity<LoggedUser> authLoginPost(@ApiParam(value = "The email/password", required = true) @Valid @RequestBody Login credentials, Errors errors) throws ApiException {

        //check if we have validation errors in credentials
        if (errors.hasErrors()) {
            throw new ApiException(errors.getFieldError().getField(), 400);
        }

        LoggedUser loggedUser = authService.authLoginPost(credentials);
        return new ResponseEntity<>(loggedUser, HttpStatus.OK);
    }

    /**
     * Register a new user in system.
     *
     * @param credentials user registration credentails(e-mail, password, first
     * & last name).
     * @param errors Errors from credentials validation.
     * @return returns HttpStatus.Ok if user created successfully else returns
     * HttpStatus.BAD_REQUEST
     * @throws org.bootcamp.yum.api.ApiException
     */
    @Override
    public ResponseEntity<Void> authRegisterPost(@ApiParam(value = "The account details", required = true) @Valid @RequestBody AccountDetails credentials, Errors errors) throws ApiException {

        //check if we have validation errors in credentials
        if (errors.hasErrors()) {
            throw new ApiException(errors.getFieldError().getField(), 400);
        }

        authService.authRegisterPost(credentials);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }

    /**
     * change user password
     *
     * @param changePassword request body params obj that contains the reset
     * password secret and the new password
     * @param errors validation errors over chnagePassword object
     * @return
     * @throws ApiException
     */
    @Override
    public ResponseEntity<Void> authChangepwdPut(@ApiParam(value = "Contains the secret and the new password", required = true) @Valid @RequestBody ChangePassword changePassword, Errors errors) throws ApiException {

        if (errors.hasErrors()) {
            throw new ApiException(errors.getFieldError().getField(), 400);
        }

        try {
            authService.authChangepwdPut(changePassword);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (OptimisticLockException ex) {
            throw new ApiException(400, "Concurrent modification exception");
        }
    }

    public ResponseEntity<TermsAndPolicy> authTermsGet() {

        TermsAndPolicy termsAndPolicy = authService.authTermsGet();

        return new ResponseEntity<TermsAndPolicy>(termsAndPolicy, HttpStatus.OK);
    }

}
