/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.OptimisticLockException;
import javax.validation.constraints.NotNull;
import javax.validation.Valid;

import org.joda.time.LocalDate;

import io.swagger.annotations.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import org.bootcamp.yum.api.model.hungry.DailyMenu;
import org.bootcamp.yum.api.model.chef.DailyOrdersChef;
import org.bootcamp.yum.api.model.chef.DailyOrders;
import org.bootcamp.yum.api.model.hungry.MenuInfo;
import org.bootcamp.yum.api.model.hungry.OrderDetails;
import org.bootcamp.yum.service.OrdersService;
import org.bootcamp.yum.api.model.hungry.UpdateDailyOrder;
import org.bootcamp.yum.api.model.Version;
import org.bootcamp.yum.exception.ConcurrentModificationException;
import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.validator.PathParamValidator;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-04T17:40:53.300+03:00")

@Controller
public class OrdersApiController implements OrdersApi {

    private OrdersService orderService;

    @Autowired
    OrdersApiController(OrdersService orderService) {
        this.orderService = orderService;
    }

    @PreAuthorize("hasAuthority('hungry')")
    @Override
    public ResponseEntity<Void> ordersIdDelete(@ApiParam(value = "The order's unique id.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "Holds information about the related menu (menu id, menu version, date).", required = true) @Valid @RequestBody MenuInfo menuInfo, Errors errors) throws ApiException {
        if (id < 1 || errors.hasErrors()) {
            System.out.println(errors.getAllErrors());
            throw new ApiException(400, "Bad request");
        }
        orderService.ordersIdDelete(id, menuInfo);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }

    @PreAuthorize("hasAuthority('hungry')")
    @Override
    public ResponseEntity<Version> ordersIdPut(@ApiParam(value = "Daily Order's Id.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The updated fields of the selected Daily Order object.", required = true) @Valid @RequestBody UpdateDailyOrder updateDailyOrder, Errors errors) throws ApiException {

        if (id < 1 || errors.hasErrors()) {
            System.out.println(errors.getAllErrors());
            throw new ApiException(errors.getFieldError().getField(), 400);
        }
        try {

            Version version = orderService.ordersIdPut(id, updateDailyOrder);
            return new ResponseEntity<>(version, HttpStatus.OK);

        } catch (OptimisticLockException ex) {
            try {
                DailyMenu menu = orderService.ordersIdGet(id, updateDailyOrder.getMenuId(), updateDailyOrder.getVersion(), updateDailyOrder.getDate());
                throw new ConcurrentModificationException(menu);
            } catch (ApiException ex1) {
                Logger.getLogger(OrdersApiController.class.getName()).log(Level.SEVERE, null, ex1);
                throw new ApiException(500, "Concurrent modification exception internal error");
            }
        }
    }

    @PreAuthorize("hasAuthority('hungry')")
    @Override
    public ResponseEntity<DailyMenu> ordersIdGet(@ApiParam(value = "The order's unique id", required = true) @PathVariable("id") Long id,
            @NotNull @ApiParam(value = "The menu's unique id", required = true) @RequestParam(value = "menuId", required = true) Long menuId,
            @NotNull @ApiParam(value = "The menu's version", required = true) @RequestParam(value = "menuVersion", required = true) Integer menuVersion,
            @NotNull @ApiParam(value = "The menu's date", required = true) @RequestParam(value = "date", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) throws ApiException {
        if (id < 1 || menuId < 1 || menuVersion < 0) {
            throw new ApiException(400, "Bad Request");
        }
        DailyMenu orderedMenu = orderService.ordersIdGet(id, menuId, menuVersion, date);
        return new ResponseEntity<>(orderedMenu, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('hungry')")
    @Override
    public ResponseEntity<DailyMenu> ordersPost(@ApiParam(value = "An array of order items (food id, quantity) for the specific date.") @Valid @RequestBody OrderDetails orderDetails, Errors errors) throws ApiException {
        if (errors.hasErrors()) {
            System.out.println(errors.getAllErrors());
            throw new ApiException(errors.getFieldError().getField(), 400);
        }

        DailyMenu orderedMenu = orderService.ordersPost(orderDetails);
        return new ResponseEntity<>(orderedMenu, HttpStatus.OK);

    }

    @PreAuthorize("hasAuthority('chef')")
    @Override
    public ResponseEntity<DailyOrders> ordersDailyDayGet(@ApiParam(value = "The requested day.", required = true) @PathVariable("day") @DateTimeFormat(pattern = "YYYY-MM-dd") LocalDate day) throws ApiException {

        return new ResponseEntity<DailyOrders>(orderService.ordersDailyDayGet(day), HttpStatus.OK);

    }

    @PreAuthorize("hasAuthority('chef')")
    @Override
    public ResponseEntity<DailyOrdersChef> ordersMonthlyGet() throws ApiException {
        DailyOrdersChef dailyOrdersChef = orderService.ordersMonthlyGet();
        return new ResponseEntity<DailyOrdersChef>(dailyOrdersChef, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('chef')")
    @Override
    public ResponseEntity<DailyOrdersChef> ordersMonthlyMonthYearGet(@ApiParam(value = "The requested month-Year in format MM-YYYY.", required = true) @PathVariable("monthYear") String monthYear) throws ApiException {

        int[] monthYearIntArray = PathParamValidator.validateWeekYear(monthYear);
        DailyOrdersChef dailyOrdersChef = orderService.ordersMonthlyMonthYearGet(monthYearIntArray);
        return new ResponseEntity<DailyOrdersChef>(dailyOrdersChef, HttpStatus.OK);

    }
}
