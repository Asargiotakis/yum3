/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api.model.chef;

import java.util.List;
import java.util.Objects;
import java.util.ArrayList;

import io.swagger.annotations.ApiModelProperty;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * FoodItems
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-27T16:35:28.866+03:00")

public class FoodItems {

    @JsonProperty("foodsVersion")
    private Integer foodsVersion = null;

    @JsonProperty("totalPagesNumber")
    private Integer totalPagesNumber = null;

    @JsonProperty("foodItems")
    private List<FoodItem> foodItems = new ArrayList<FoodItem>();

    public FoodItems foodsVersion(Integer foodsVersion) {
        this.foodsVersion = foodsVersion;
        return this;
    }

    /**
     * Get foodsVersion
     *
     * @return foodsVersion
     *
     */
    @ApiModelProperty(value = "")
    public Integer getFoodsVersion() {
        return foodsVersion;
    }

    public void setFoodsVersion(Integer foodsVersion) {
        this.foodsVersion = foodsVersion;
    }

    public FoodItems totalPagesNumber(Integer totalPagesNumber) {
        this.totalPagesNumber = totalPagesNumber;
        return this;
    }

    public FoodItems foodItems(List<FoodItem> foodItems) {
        this.foodItems = foodItems;
        return this;
    }

    public FoodItems addFoodItemsItem(FoodItem foodItemsItem) {
        this.foodItems.add(foodItemsItem);
        return this;
    }

    /**
     * Get foodItems
     *
     * @return foodItems
     *
     */
    @ApiModelProperty(value = "")
    public List<FoodItem> getFoodItems() {
        return foodItems;
    }

    public void setFoodItems(List<FoodItem> foodItems) {
        this.foodItems = foodItems;
    }

    /**
     * Get totalPagesNumber
     *
     * @return totalPagesNumber
     */
    @ApiModelProperty(value = "")
    public Integer getTotalPagesNumber() {
        return totalPagesNumber;
    }

    public void setTotalPagesNumber(Integer totalPagesNumber) {
        this.totalPagesNumber = totalPagesNumber;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FoodItems foodItems = (FoodItems) o;
        return Objects.equals(this.foodsVersion, foodItems.foodsVersion)
                && Objects.equals(this.totalPagesNumber, foodItems.totalPagesNumber)
                && Objects.equals(this.foodItems, foodItems.foodItems);
    }

    @Override
    public int hashCode() {
        return Objects.hash(foodsVersion, foodItems);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class FoodItems {\n");

        sb.append("    foodsVersion: ").append(toIndentedString(foodsVersion)).append("\n");
        sb.append("    totalPagesNumber: ").append(toIndentedString(totalPagesNumber)).append("\n");
        sb.append("    foodItems: ").append(toIndentedString(foodItems)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
