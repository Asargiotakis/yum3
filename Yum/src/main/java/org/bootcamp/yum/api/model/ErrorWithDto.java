/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api.model;

import java.util.Objects;

import io.swagger.annotations.ApiModelProperty;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * ErrorWithDto
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-16T13:04:09.957+03:00")

public class ErrorWithDto {

    @JsonProperty("message")
    private String message = null;

    @JsonProperty("error")
    private String error = null;

    @JsonProperty("dto")
    private Object dto = null;

    public ErrorWithDto message(String message) {
        this.message = message;
        return this;
    }

    /**
     * Get message
     *
     * @return message
     *
     */
    @ApiModelProperty(value = "")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ErrorWithDto error(String error) {
        this.error = error;
        return this;
    }

    /**
     * Get error
     *
     * @return error
     *
     */
    @ApiModelProperty(value = "")
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public ErrorWithDto dto(Object dto) {
        this.dto = dto;
        return this;
    }

    /**
     * Get dto
     *
     * @return dto
     *
     */
    @ApiModelProperty(value = "")
    public Object getDto() {
        return dto;
    }

    public void setDto(Object dto) {
        this.dto = dto;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ErrorWithDto errorWithDto = (ErrorWithDto) o;
        return Objects.equals(this.message, errorWithDto.message)
                && Objects.equals(this.error, errorWithDto.error)
                && Objects.equals(this.dto, errorWithDto.dto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message, error, dto);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ErrorWithDto {\n");

        sb.append("    message: ").append(toIndentedString(message)).append("\n");
        sb.append("    error: ").append(toIndentedString(error)).append("\n");
        sb.append("    dto: ").append(toIndentedString(dto)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
