/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;

import javax.validation.Valid;

import io.swagger.annotations.*;

import org.springframework.validation.Errors;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;

import org.bootcamp.yum.api.model.chef.FoodItem;
import org.bootcamp.yum.api.model.chef.FoodItems;
import org.bootcamp.yum.api.model.chef.FoodDetails;
import org.bootcamp.yum.api.model.chef.FoodItemEditable;
import org.bootcamp.yum.api.model.chef.UpdateFoodDetails;
import org.bootcamp.yum.api.model.Version;
import org.bootcamp.yum.exception.ApiException;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-27T16:35:28.866+03:00")

@Api(value = "foods", description = "the foods API")
@RequestMapping(value = "/api")
public interface FoodsApi {

    @CrossOrigin
    @ApiOperation(value = "Find a food by its name.", notes = "Returns a food with the given name.", response = FoodItem.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The requested food.", response = FoodItem.class)
        ,
        @ApiResponse(code = 404, message = "Food not found or archived.", response = FoodItem.class)})
    @RequestMapping(value = "/foods/findByName/{name}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<FoodItem> foodsFindByNameNameGet(@ApiParam(value = "The name of the requested food.", required = true) @PathVariable("name") String name) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Gets all foods.", notes = "Returns a list of all the foods.", response = FoodItems.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "A list of foods.", response = FoodItems.class)
        ,
        @ApiResponse(code = 304, message = "List of foods not modified.", response = FoodItems.class)
        ,
        @ApiResponse(code = 400, message = "Bad request.", response = FoodItems.class)})
    @RequestMapping(value = "/foods",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<FoodItems> foodsGet(@ApiParam(value = "A string to describe the client's foods version.") @RequestParam(value = "version", required = false) Integer version,
            @ApiParam(value = "A boolean to describe if the client requests the archived foods.") @RequestParam(value = "archived", required = false) Boolean archived,
            @ApiParam(value = "A string to describe the filtering.") @RequestParam(value = "type", required = false) String type,
            @ApiParam(value = "A string to describe the sorting preference.") @RequestParam(value = "sort", required = false) String sort,
            @ApiParam(value = "An integer to describe the requested page.") @RequestParam(value = "page", required = false) Integer page,
            @ApiParam(value = "An integer to describe the number of foods to retrieve.") @RequestParam(value = "size", required = false) Integer size) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Delete or archive the food with the given id.", notes = "Delete or archive a food by its id.", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "OK", response = Void.class)
        ,
        @ApiResponse(code = 400, message = "Bad request.", response = Void.class)
        ,
        @ApiResponse(code = 404, message = "Food not found.", response = Void.class)
        ,
        @ApiResponse(code = 412, message = "Food cannot be deleted.", response = Void.class)})
    @RequestMapping(value = "/foods/{id}",
            produces = {"application/json"},
            method = RequestMethod.DELETE)
    ResponseEntity<Void> foodsIdDelete(@ApiParam(value = "The food's unique id.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "A boolean to describe if the Food object has to be archived.") @RequestParam(value = "archived", required = false) Boolean archived) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Checks if a food is editable.", notes = "Gets the food_id and checks if it can be edited.", response = FoodItemEditable.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "A Food found by id", response = FoodItemEditable.class)
        ,
        @ApiResponse(code = 400, message = "Bad request.", response = FoodItemEditable.class)
        ,
        @ApiResponse(code = 404, message = "Food with that id, is not found.", response = FoodItemEditable.class)})
    @RequestMapping(value = "/foods/{id}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<FoodItemEditable> foodsIdGet(@ApiParam(value = "The food's unique id", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "A boolean to describe if the Food object is editable.") @RequestParam(value = "editable", required = false) Boolean editable) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Updates the food with the given id", notes = "Updates the fields of a food object with the specified id.", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK.", response = Version.class)
        ,
        @ApiResponse(code = 400, message = "Bad request.", response = Void.class)
        ,
        @ApiResponse(code = 404, message = "Food with that id, is not found.", response = Void.class)
        ,
        @ApiResponse(code = 409, message = "Concurrent modification error.", response = FoodItemEditable.class)})
    @RequestMapping(value = "/foods/{id}",
            produces = {"application/json"},
            method = RequestMethod.PUT)
    ResponseEntity<Object> foodsIdPut(@ApiParam(value = "The food's unique id.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The updated fields of the selected food object.", required = true) @RequestBody UpdateFoodDetails updateFoodDetails,
            @ApiParam(value = "A boolean to describe if the Food object has to be cloned.") @RequestParam(value = "clone", required = false) Boolean clone) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Adds a new food to the system.", notes = "Adds a new food to the system with the given characteristics.", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Food created successfully.", response = Void.class)
        ,
        @ApiResponse(code = 400, message = "Food could not be created.", response = Void.class)
        ,
        @ApiResponse(code = 412, message = "Food name already exists.", response = Void.class)})
    @RequestMapping(value = "/foods",
            produces = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<Void> foodsPost(@ApiParam(value = "The food to create.") @Valid @RequestBody FoodDetails foodDetails, Errors errors) throws ApiException;

}
