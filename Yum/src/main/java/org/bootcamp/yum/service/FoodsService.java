/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.service;

import java.util.List;
import java.util.ArrayList;
import java.math.BigDecimal;

import org.joda.time.LocalDate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.PageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import org.bootcamp.enums.FoodEnum;
import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.exception.ConcurrentModificationException;
import org.bootcamp.yum.data.entity.Food;
import org.bootcamp.yum.api.model.Version;
import org.bootcamp.yum.api.model.chef.FoodItem;
import org.bootcamp.yum.api.model.chef.FoodItems;
import org.bootcamp.yum.data.entity.OrderItem;
import org.bootcamp.yum.api.model.chef.FoodDetails;
import org.bootcamp.yum.data.entity.DailyMenu;
import org.bootcamp.yum.api.model.chef.FoodItemEditable;
import org.bootcamp.yum.api.model.chef.UpdateFoodDetails;
import org.bootcamp.yum.data.repository.FoodRepository;
import org.bootcamp.yum.data.repository.DailyMenuRepository;
import org.bootcamp.yum.data.repository.OrderItemRepository;

@Service
public class FoodsService {

    @Autowired
    private FoodRepository foodRepository;
    @Autowired
    private OrderItemRepository orderItemRepository;
    @Autowired
    private DailyMenuRepository dailyMenuRepository;

    @Autowired
    private YumService yumService;

    /**
     * That method GETs all the available foods.
     *
     * @param version given foods list version
     * @param archived request for archived or active foods
     * @param type filtering type
     * @param sort sorting preference
     * @param size requested list size by page
     * @param page requested page
     * @return An arraylist of foods
     */
    public FoodItems foodsGet(int version, boolean archived, String type, String sort, int page, int size) throws ApiException {

        //if we have version in query params 
        if (version != -1) {
            //check given version with the current foods list version
            //if there is a new version return the foods list, else throw ApiException
            if (isFoodsListupdated(version)) {
                return getAllFoods();
            } else {
                throw new ApiException(304, "The list of foods has not modified.");
            }
        }
        //if we have page and size(and not type, sort) in query params
        //then return a paged(list that contains the foods that follow the page number and size rule) food list 
        if (type.isEmpty() && sort.isEmpty()) {

            Page<Food> findByArchivedPage = foodRepository.findByArchived(archived, new PageRequest(page, size, Sort.Direction.DESC, "id"));
            return setupPagedFoodsList(findByArchivedPage.getContent(), findByArchivedPage.getTotalPages());

        } else //check if we have the type in query params
        if (!type.isEmpty() && sort.isEmpty()) {

            try {
                //check that the give type is valid
                type = type.trim().toUpperCase();
                FoodEnum.valueOf(type);
            } catch (IllegalArgumentException ex) {
                throw new ApiException(400, "Bad request.");
            }

            Page<Food> findByArchivedAndTypePage = foodRepository.findByArchivedAndType(archived, FoodEnum.valueOf(type),
                    new PageRequest(page, size, Sort.Direction.DESC, "id"));

            return setupPagedFoodsList(findByArchivedAndTypePage.getContent(), findByArchivedAndTypePage.getTotalPages());

        } else //check if we have the sort in query params
        if (type.isEmpty() && !sort.isEmpty()) {

            sort = sort.trim().toLowerCase();
            Page<Food> findByArchivedAndSortPage;

            switch (sort) {

                case "lowestprice":
                    findByArchivedAndSortPage = foodRepository.findByArchived(archived,
                            new PageRequest(page, size, Sort.Direction.ASC, "price"));

                    return setupPagedFoodsList(findByArchivedAndSortPage.getContent(), findByArchivedAndSortPage.getTotalPages());
                case "highestprice":
                    findByArchivedAndSortPage = foodRepository.findByArchived(archived,
                            new PageRequest(page, size, Sort.Direction.DESC, "price"));

                    return setupPagedFoodsList(findByArchivedAndSortPage.getContent(), findByArchivedAndSortPage.getTotalPages());
                default:
                    //if the given sort param is not valid
                    throw new ApiException(400, "bad request.");

            }
        } else //when we have both sorting and filtering
        if (!type.isEmpty() && !sort.isEmpty()) {

            try {
                type = type.trim().toUpperCase();
                FoodEnum.valueOf(type);
            } catch (IllegalArgumentException ex) {
                throw new ApiException(400, "bad request.");
            }

            sort = sort.trim().toLowerCase();
            Page<Food> findByArchivedAndTypeAndSortPage;

            switch (sort) {

                case "lowestprice":
                    findByArchivedAndTypeAndSortPage = foodRepository.findByArchivedAndType(archived,
                            FoodEnum.valueOf(type),
                            new PageRequest(page, size, Sort.Direction.ASC, "price"));
                    return setupPagedFoodsList(findByArchivedAndTypeAndSortPage.getContent(), findByArchivedAndTypeAndSortPage.getTotalPages());
                case "highestprice":
                    findByArchivedAndTypeAndSortPage = foodRepository.findByArchivedAndType(archived,
                            FoodEnum.valueOf(type),
                            new PageRequest(page, size, Sort.Direction.DESC, "price"));
                    return setupPagedFoodsList(findByArchivedAndTypeAndSortPage.getContent(), findByArchivedAndTypeAndSortPage.getTotalPages());
                default:
                    throw new ApiException(400, "bad request.");
            }

        }
        return null;
    }

    /**
     * That method GETs a specified food by id and checks if it can be edited or
     * not.
     *
     * @param id The requested food's id.
     * @return The requested FoodItem
     * @throws ApiException
     */
    public FoodItemEditable foodsIdGet(Long id) throws ApiException {

        Food requestedFood = foodRepository.findById(id);
        //check if the food does not exists 
        if (requestedFood == null) {
            throw new ApiException(404, "Food not found");
        }

        return getFoodItemEditable(requestedFood);
//        //FoodItem Instantiation
//        FoodItem foodItem = new FoodItem(requestedFood.getId(),
//                requestedFood.getType().name(),
//                requestedFood.getName(),
//                requestedFood.getDescription(),
//                requestedFood.getPrice(),
//                requestedFood.isArchived()
//        );
//
//        return new FoodItemEditable(foodItem, isEditable(requestedFood), requestedFood.getVersion());

    }

    /**
     * That method updates the details of a required food.
     *
     * @param id The requested food's id
     * @param updateFoodDetails The new food details
     * @param clone That boolean indicates, if the user wants the required food
     * item to be cloned or not.
     * @return
     * @throws ApiException
     */
    @Transactional
    public Version foodsIdPut(Long id, UpdateFoodDetails updateFoodDetails, Boolean clone) throws ApiException {

        Food food = foodRepository.findOne(id);
        boolean canClone = canClone(clone);
        if (food == null) {
            throw new ApiException(404, "Food not found");
        }

        int requestFoodVersion = updateFoodDetails.getVersion();

        if (requestFoodVersion != food.getVersion()) {
//            throw new ApiException(409, "Conflict");
            throw new ConcurrentModificationException(getFoodItemEditable(food));
        }

        if (food.isArchived() && !canClone) {
            throw new ApiException(400, "Bad Request, food is archived");
        }

        ArrayList<OrderItem> orderItems = (ArrayList<OrderItem>) orderItemRepository.findByFoodId(id);

        Food clonedFood = null;
        boolean isModified = false;
        // if the food can not be cloned
        if (!canClone) {
            // check if the food can be modified
            isModified = food.updateFields(updateFoodDetails.getFoodDetails(), orderItems.isEmpty());
            if (!isModified) {
                throw new ApiException(409, "Conflict");
            }
        } else {
            food.setArchived(true);
            List<DailyMenu> futureDailyMenus = getFutureDailyMenusByFoodId(id);
            //check if the food exists in futured daily menus 
            if (!futureDailyMenus.isEmpty()) {
                removeFoodFromDailyMenus(futureDailyMenus, food);
            }
            clonedFood = cloneFood(updateFoodDetails);
        }
        // if the food has not been modified the cloned  
        Version version = new Version();
        if (!isModified) {
            foodRepository.save(clonedFood);
            version.setVersion(food.getVersion());
        } else {
            version.setVersion(food.getVersion() + 1);
        }

        // Update foods list version
        yumService.updateFoodsVersion();

        return version;
    }

    /**
     * That method GETs a food by name
     *
     * @param name The requested food's name
     * @return The requested FoodITtem
     */
    public FoodItem foodsFindByNameNameGet(String name) throws ApiException {

        Food food = foodRepository.findByName(name.trim());
        FoodItem foodItem1 = new FoodItem();
        if (food != null) {
            FoodItem foodItem = new FoodItem(food.getId(),
                    food.getType().name(),
                    food.getName(),
                    food.getDescription(),
                    food.getPrice(),
                    food.isArchived()
            );
            return foodItem;
        } else {
            throw new ApiException(404, "Food not found");
        }
    }

    /**
     * Creates a new food
     *
     * @param foodDetails The food details
     * @throws ApiException
     */
    @Transactional
    public void foodsPost(FoodDetails foodDetails) throws ApiException {

        /*
         * Validate foodDetails
         */
        // Check if required fields are not null or empty
        if (foodDetails.getFoodType().trim().isEmpty()
                || foodDetails.getName().trim().isEmpty()
                || foodDetails.getPrice().doubleValue() < 0.01) {
            throw new ApiException(400, "Bad Request");
        }

        // Try to parse food type enum
        try {
            FoodEnum.valueOf(foodDetails.getFoodType().trim().toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new ApiException(400, "Bad Request");
        }

        // Check if the food name exists in active foods list
        Food foodToCheckArchive = foodRepository.findByName(foodDetails.getName());
        if (foodToCheckArchive != null && !foodToCheckArchive.isArchived()) {
            throw new ApiException(412, "Food already exists");
        }

        /*
         * Create new food, set its properties and store it in the database
         */
        Food food = new Food();
        food.setType(FoodEnum.valueOf(foodDetails.getFoodType().trim().toUpperCase()));
        food.setName(foodDetails.getName().trim());
        if (foodDetails.getDescription() != null) {
            food.setDescription(foodDetails.getDescription().trim());
        }
        food.setPrice(foodDetails.getPrice());

        // Save it in the database
        foodRepository.save(food);

        // Update foods list version
        yumService.updateFoodsVersion();

    }

    /**
     * That method deletes a specified by id food
     *
     * @param id The requested food's id
     * @param archived A boolean that indicates if a food is archived or not
     */
    @Transactional
    public void foodsIdDelete(Long id, Boolean archived) throws ApiException {

        Food food = foodRepository.findOne(id);
        /*
         *  check if food exists
         */
        if (food == null) {
            throw new ApiException(404, "Food not found");
        }

        /*
         *  check if food is archived
         */
        if (food.isArchived()) {
            throw new ApiException(400, "Bad Request");
        }

        List<OrderItem> orderItems = orderItemRepository.findByFoodId(id);
        List<DailyMenu> dailyMenus = getFutureDailyMenusByFoodId(id);

        //  check if food is not ordered and archived param is not setted
        if (!orderItems.isEmpty() && archived == null) {
            throw new ApiException(412, "Precondition Failed");
        } else // check if food is  ordered and not contained in  future dailyMenus and is set to archive
        if (!orderItems.isEmpty() && dailyMenus.isEmpty() && archived) {
            food.setArchived(true);
        } else // check if food is  ordered and  contained in future dailyMenus and is set to archive
        if (!orderItems.isEmpty() && !dailyMenus.isEmpty() && archived) {
            removeFoodFromDailyMenus(dailyMenus, food);
            food.setArchived(true);

        } else // check if food is not ordered 
        if (orderItems.isEmpty()) {
            foodRepository.delete(id);
        }

        // Update foods list version
        yumService.updateFoodsVersion();
    }

    /*
     *check the daily menus from today and after if we have orders for the given food. 
     */
    private void removeFoodFromDailyMenus(List<DailyMenu> dailyMenus, Food food) {

        //loops throw all the daily menus from today and after
        for (DailyMenu dailyMenu : dailyMenus) {
            List<OrderItem> orderItems = orderItemRepository.findByFoodIdAndDailyOrderIn(food.getId(), dailyMenu.getDailyOrders());

            // check if the food is ordered in the current daily menu
            if (orderItems.isEmpty()) {
                food.removeFromFutureDailyMenus(dailyMenu);
            }
        }
    }

    private boolean canClone(Boolean clone) {
        return clone != null && clone;
    }

    private Food cloneFood(UpdateFoodDetails updateFoodDetails) {
        Food food = new Food();
        String foodName = updateFoodDetails.getFoodDetails().getName().trim();
        BigDecimal foodPrice = new BigDecimal(updateFoodDetails.getFoodDetails().getPrice().toString());
        String foodType = updateFoodDetails.getFoodDetails().getFoodType().trim().toUpperCase();
        String foodDescription = updateFoodDetails.getFoodDetails().getDescription().trim();
        food.setArchived(false);
        food.setName(foodName);
        food.setPrice(foodPrice);
        food.setType(FoodEnum.valueOf(foodType));
        food.setDescription(foodDescription);
        return food;
    }

    private List<DailyMenu> getFutureDailyMenusByFoodId(long foodId) {
        return dailyMenuRepository.findByOfDateGreaterThanEqualAndFoodsId(LocalDate.now(), foodId);
    }

    /*
     * That method checks if a food can be edited or not and returns a boolean
     */
    private boolean isEditable(Food requestedFood) {
        //check if the food is not in the orderItems list and if is not archived
        if (orderItemRepository.findByFoodId(requestedFood.getId()).isEmpty() && !requestedFood.isArchived()) {
            return true;
        } else {
            return false;
        }
    }

    public FoodItems getAllFoods() {
        ArrayList<FoodItem> responseFoodslist = new ArrayList<>();

        Iterable<Food> foods = foodRepository.findAll();
        for (Food food : foods) {
            FoodItem foodItem = new FoodItem(food.getId(),
                    food.getType().name(),
                    food.getName(),
                    food.getDescription(),
                    food.getPrice(),
                    food.isArchived());

            responseFoodslist.add(foodItem);
        }
        FoodItems foodItems = new FoodItems();
        foodItems.setFoodItems(responseFoodslist);
        foodItems.setFoodsVersion(yumService.getFoodsVersion());
        return foodItems;
    }

    public boolean isFoodsListupdated(Integer version) {

        if (version != -1 && yumService.getFoodsVersion() > version) {
            return true;
        }
        return false;
    }

    private FoodItems setupPagedFoodsList(List<Food> content, int totalPages) {
        ArrayList<FoodItem> responseFoodslist = new ArrayList<>();
        for (Food food : content) {
            FoodItem foodItem = new FoodItem(food.getId(),
                    food.getType().name(),
                    food.getName(),
                    food.getDescription(),
                    food.getPrice(),
                    food.isArchived());

            responseFoodslist.add(foodItem);
        }

        FoodItems foodItems = new FoodItems();
        foodItems.setFoodItems(responseFoodslist);
        foodItems.setTotalPagesNumber(totalPages);
        foodItems.setFoodsVersion(yumService.getFoodsVersion());

        return foodItems;
    }

    public FoodItems getDefaultFoodList() {
        Page<Food> findByArchivedPage = foodRepository.findByArchived(false, new PageRequest(0, 10, Sort.Direction.DESC, "id"));
        return setupPagedFoodsList(findByArchivedPage.getContent(), findByArchivedPage.getTotalPages());

    }

    public Version getFoodVersionById(Long id) {
        Version version = new Version();
        version.setVersion(foodRepository.findById(id).getVersion());
        return version;
    }

    private FoodItemEditable getFoodItemEditable(Food food) {

        //FoodItem Instantiation
        FoodItem foodItem = new FoodItem(food.getId(),
                food.getType().name(),
                food.getName(),
                food.getDescription(),
                food.getPrice(),
                food.isArchived()
        );

        return new FoodItemEditable(foodItem, isEditable(food), food.getVersion());

    }

}
