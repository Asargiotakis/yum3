/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import org.springframework.boot.test.context.SpringBootTest;

import org.bootcamp.yum.api.AuthApiControllerTest;
import org.bootcamp.yum.api.DailyMenusApiControllerTest;
import org.bootcamp.yum.api.FoodsApiControllerTest;
import org.bootcamp.yum.api.GlobalsettingsApiControllerTest;
import org.bootcamp.yum.api.MenusApiControllerTest;
import org.bootcamp.yum.api.OrdersApiControllerTest;
import org.bootcamp.yum.api.UsersApiControllerTest;

@SpringBootTest
@RunWith(Suite.class)
@Suite.SuiteClasses({
    AuthApiControllerTest.class,
    DailyMenusApiControllerTest.class,
    FoodsApiControllerTest.class,
    GlobalsettingsApiControllerTest.class,
    MenusApiControllerTest.class,
    OrdersApiControllerTest.class,
    UsersApiControllerTest.class
})
public class YumApplicationTests {

    @Test
    public void contextLoads() {
    }

}
