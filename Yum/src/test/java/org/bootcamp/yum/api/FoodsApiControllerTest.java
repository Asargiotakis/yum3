/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;

import java.util.List;
import java.util.ArrayList;
import java.math.BigDecimal;

import org.joda.time.LocalDate;

import org.junit.Test;
import org.junit.Rule;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.anyBoolean;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import static org.hamcrest.Matchers.*;

import org.springframework.http.MediaType;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.bootcamp.enums.FoodEnum;
import org.bootcamp.test.MockAuthRule;
import org.bootcamp.yum.data.entity.Yum;
import org.bootcamp.yum.data.entity.Food;
import org.bootcamp.yum.data.entity.OrderItem;
import org.bootcamp.yum.data.entity.OrderItemKey;
import org.bootcamp.yum.data.repository.FoodRepository;
import org.bootcamp.yum.data.repository.DailyMenuRepository;
import org.bootcamp.yum.data.repository.OrderItemRepository;
import org.bootcamp.yum.exception.ExceptionControllerAdvice;
import org.bootcamp.yum.service.YumService;
import org.bootcamp.yum.service.FoodsService;

@RunWith(MockitoJUnitRunner.class)
public class FoodsApiControllerTest {

    @Rule
    public final MockAuthRule mockAuth = new MockAuthRule();

    @InjectMocks
    private final FoodsService foodsService = new FoodsService();

    @Mock
    private final YumService yumService = new YumService();

    @Mock
    private FoodRepository mockFoodRepository;

    @Mock
    private OrderItemRepository orderItemRepository;

    @Mock
    private OrderItemRepository mockOrderItemRepository;

    @Mock
    private DailyMenuRepository dailyMenuRepository;

    private MockMvc mockMvc;

    private static List<Food> mockFoodList;
    private static Food mockFood;
    private static Food mockFoodPut;
    private static List<OrderItem> mockOrderItemsList;
    private static OrderItem mockOrderItem;
    private static OrderItemKey mockOrderItemKey;
    private static Yum mockYum;
    private static PageImpl<Food> mockPage;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(new FoodsApiController(foodsService))
                .setControllerAdvice(new ExceptionControllerAdvice())
                .build();
    }

    @BeforeClass
    public static void initMockData() {
        // Mock List of Foods
        mockFoodList = new ArrayList<>();
        // Adding the first mock Food instance
        Food mockFood1 = new Food(10);
        mockFood1.setArchived(true);
        mockFood1.setDescription("test Pastitsio");
        mockFood1.setName("Pastitsio");
        mockFood1.setType(FoodEnum.MAIN);
        mockFood1.setPrice(new BigDecimal("5.65"));
        mockFood1.setVersion(1);
        mockFoodList.add(mockFood1);

        Food mockFood2 = new Food(1);
        mockFood2.setArchived(true);
        mockFood2.setDescription("test Pastitsio2");
        mockFood2.setName("Pastitsio2");
        mockFood2.setType(FoodEnum.MAIN);
        mockFood2.setPrice(new BigDecimal("5.65"));
        mockFood2.setVersion(1);
        mockFoodList.add(mockFood2);

        mockFood = new Food(0);
        mockFood.setDescription("test Mousaka");
        mockFood.setName("Mousaka");
        mockFood.setType(FoodEnum.MAIN);
        mockFood.setArchived(false);
        mockFood.setPrice(new BigDecimal("5.00"));
        mockFood.setVersion(0);
        //mockFoodList.add(mockFood);

        //Mock the OrderItems list
        mockOrderItemsList = new ArrayList<>();
        //Mock the OrderItemKey
        mockOrderItemKey = new OrderItemKey(1L, 10L);
        //Mock the OrderItem
        mockOrderItem = new OrderItem(mockOrderItemKey, 1);
        mockOrderItem.setFood(mockFood1);
        mockOrderItemsList.add(mockOrderItem);

        mockYum = new Yum(1);
        mockYum.setCurrency("&euro;");
        mockYum.setFoodsVersion(1);
//        mockYum.s?

        mockPage = new PageImpl<>(mockFoodList);

        mockFoodPut = new Food(12);
        mockFoodPut.setDescription("test pastitsio");
        mockFoodPut.setName("Pastitsio");
        mockFoodPut.setType(FoodEnum.MAIN);
        mockFoodPut.setArchived(false);
        mockFoodPut.setPrice(new BigDecimal("5.00"));
        mockFoodPut.setVersion(0);
    }

    /**
     * Test of testFoodsGet method, of class FoodsApiController. Case a list of
     * foods has been returned (no query params).
     */
    @Test
    public void testFoodsGet() throws Exception {

        // telling Mockito to use this mock list every time the findAll method is called on the foodRepo
        given(mockFoodRepository.findByArchived(anyBoolean(), any(Pageable.class))).willReturn(mockPage);
        // We perform the API call, and check that the response status code, and the JSON response are corrects
        mockMvc.perform(get("/api/foods")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.foodItems", hasSize(2)))
                .andExpect(jsonPath("$.foodItems[0].id", is(10)))
                .andExpect(jsonPath("$.foodItems[0].name", is("Pastitsio")))
                .andExpect(jsonPath("$.foodItems[1].id", is(1)));

        // we verify that we called findAll method once only on the repo.
        verify(mockFoodRepository, times(1)).findByArchived(anyBoolean(), any(Pageable.class));
        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(mockFoodRepository);

    }

    /**
     * Test of testFoodsGet method, of class FoodsApiController. Case a list of
     * foods has been returned (no query params).
     */
    @Test
    public void testFoodsGet_emptyList() throws Exception {

        given(mockFoodRepository.findByArchived(anyBoolean(), any(Pageable.class))).willReturn(new PageImpl<>(new ArrayList<>()));

        mockMvc.perform(get("/api/foods")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.foodItems", hasSize(0)))
                .andExpect(jsonPath("$.foodsVersion", is(0)));

        verify(mockFoodRepository, times(1)).findByArchived(anyBoolean(), any(Pageable.class));
        verifyNoMoreInteractions(mockFoodRepository);

    }

    /**
     * Test of testFoodsGet method, of class FoodsApiController. Case a list of
     * foods has returned (with version in query params).
     */
    @Test
    public void testFoodsGet_withQueryParamsVersion() throws Exception {

        given(yumService.getFoodsVersion()).willReturn(1);
        given(mockFoodRepository.findAll()).willReturn(mockFoodList);

        mockMvc.perform(get("/api/foods?version=0")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.foodItems", hasSize(2)))
                .andExpect(jsonPath("$.foodsVersion", is(1)));

        verify(mockFoodRepository, times(1)).findAll();
        verifyNoMoreInteractions(mockFoodRepository);

    }

    /**
     * Test of testFoodsGet method, of class FoodsApiController. Case foods list
     * has not modified (304) has returned (with version in query params).
     */
    @Test
    public void testFoodsGet_withQueryParamsVersion_notModified() throws Exception {

        given(yumService.getFoodsVersion()).willReturn(1);
        given(mockFoodRepository.findAll()).willReturn(null);

        mockMvc.perform(get("/api/foods?version=1")).andExpect(status().isNotModified());

        verify(mockFoodRepository, times(0)).findAll();
        verifyNoMoreInteractions(mockFoodRepository);

    }

    /**
     * Test of testFoodsGet method, of class FoodsApiController. Case returned a
     * food list that follow the page and size rule (with page and size in query
     * params).
     */
    @Test
    public void testFoodsGet_withQueryParamsPagination() throws Exception {

        given(mockFoodRepository.findByArchived(anyBoolean(), any(Pageable.class))).willReturn(mockPage);

        mockMvc.perform(get("/api/foods?page=1&size=10")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.foodItems", hasSize(2)))
                .andExpect(jsonPath("$.foodsVersion", is(0)));

        verify(mockFoodRepository, times(1)).findByArchived(anyBoolean(), any(Pageable.class));
        verifyNoMoreInteractions(mockFoodRepository);

    }

    /**
     * Test of testFoodsGet method, of class FoodsApiController. Case returned
     * an empty list (with page > valid page number and size in query params).
     */
    @Test
    public void testFoodsGet_withQueryParamsPagination_invalidPageNumber() throws Exception {

        given(mockFoodRepository.findByArchived(anyBoolean(), any(Pageable.class))).willReturn(new PageImpl<>(new ArrayList<>()));

        mockMvc.perform(get("/api/foods?page=20&size=10")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.foodItems", hasSize(0)));

        verify(mockFoodRepository, times(1)).findByArchived(anyBoolean(), any(Pageable.class));
        verifyNoMoreInteractions(mockFoodRepository);

    }

    /**
     * Test of testFoodsGet method, of class FoodsApiController. Case returned a
     * food list that follow the page and size rule and with filtered by food
     * type (with page, size and type in query params).
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testFoodsGet_withQueryParamsPaginationWithFilter() throws Exception {

        given(mockFoodRepository.findByArchivedAndType(anyBoolean(), any(FoodEnum.class), any(Pageable.class))).willReturn(mockPage);

        mockMvc.perform(get("/api/foods?type=main&page=0&size=10")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.foodItems", hasSize(2)))
                .andExpect(jsonPath("$.foodsVersion", is(0)));

        verify(mockFoodRepository, times(1)).findByArchivedAndType(anyBoolean(), any(FoodEnum.class), any(Pageable.class));
        verifyNoMoreInteractions(mockFoodRepository);

    }

    /**
     * Test of testFoodsGet method, of class FoodsApiController. Case returned
     * bad request reason invalid type
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testFoodsGet_withQueryParamsPaginationWithFilter_invalidType() throws Exception {

        given(mockFoodRepository.findByArchivedAndType(anyBoolean(), any(FoodEnum.class), any(Pageable.class))).willReturn(mockPage);

        mockMvc.perform(get("/api/foods?type=test&page=0&size=10")).andExpect(status().isBadRequest());

        verify(mockFoodRepository, times(0)).findByArchivedAndType(anyBoolean(), any(FoodEnum.class), any(Pageable.class));
        verifyNoMoreInteractions(mockFoodRepository);
    }

    /**
     * Test of testFoodsGet method, of class FoodsApiController. Case returned a
     * food list that follow the page and size rule and sorted by lowest
     * price(sort in query params, use the defaults for page and size).
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testFoodsGet_withQueryParamsPaginationWithSorting() throws Exception {

        given(mockFoodRepository.findByArchived(anyBoolean(), any(Pageable.class))).willReturn(mockPage);

        mockMvc.perform(get("/api/foods?sort=lowestprice")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.foodItems", hasSize(2)))
                .andExpect(jsonPath("$.foodsVersion", is(0)));

        verify(mockFoodRepository, times(1)).findByArchived(anyBoolean(), any(Pageable.class));
        verifyNoMoreInteractions(mockFoodRepository);
    }

    /**
     * Test of testFoodsGet method, of class FoodsApiController. Case returned
     * bad request reason invalid sorting param.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testFoodsGet_withQueryParamsPaginationWithSorting_invalidSortParam() throws Exception {

        given(mockFoodRepository.findByArchived(anyBoolean(), any(Pageable.class))).willReturn(mockPage);

        mockMvc.perform(get("/api/foods?sort=test")).andExpect(status().isBadRequest());

        verify(mockFoodRepository, times(0)).findByArchived(anyBoolean(), any(Pageable.class));
        verifyNoMoreInteractions(mockFoodRepository);
    }

    /**
     * Test of testFoodsGet method, of class FoodsApiController. Case returned a
     * food list that follow the page and size rule and sorted by lowest
     * price(with page, size, sort and type in query params).
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testFoodsGet_withQueryParamsPaginationWithSortingAndFiltering() throws Exception {

        given(mockFoodRepository.findByArchivedAndType(anyBoolean(), any(FoodEnum.class), any(Pageable.class))).willReturn(mockPage);

        mockMvc.perform(get("/api/foods?type=main&sort=lowestprice&page=0&size=10")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.foodItems", hasSize(2)))
                .andExpect(jsonPath("$.foodsVersion", is(0)));

        verify(mockFoodRepository, times(1)).findByArchivedAndType(anyBoolean(), any(FoodEnum.class), any(Pageable.class));
        verifyNoMoreInteractions(mockFoodRepository);
    }

    /**
     * Test of testFoodsGet method, of class FoodsApiController. Case returned
     * bad request reason invalid food type.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testFoodsGet_withQueryParamsPaginationWithSortingAndFiltering_invalidType() throws Exception {

        given(mockFoodRepository.findByArchivedAndType(anyBoolean(), any(FoodEnum.class), any(Pageable.class))).willReturn(mockPage);

        mockMvc.perform(get("/api/foods?type=test&sort=lowestprice&page=0&size=10")).andExpect(status().isBadRequest());

        verify(mockFoodRepository, times(0)).findByArchivedAndType(anyBoolean(), any(FoodEnum.class), any(Pageable.class));
        verifyNoMoreInteractions(mockFoodRepository);
    }

    /**
     * Test of testFoodsGet method, of class FoodsApiController. Case returned
     * bad request reason invalid sort param.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testFoodsGet_withQueryParamsPaginationWithSortingAndFiltering_invalidSortParam() throws Exception {

        given(mockFoodRepository.findByArchivedAndType(anyBoolean(), any(FoodEnum.class), any(Pageable.class))).willReturn(mockPage);

        mockMvc.perform(get("/api/foods?type=main&sort=test&page=0&size=10")).andExpect(status().isBadRequest());

        verify(mockFoodRepository, times(0)).findByArchivedAndType(anyBoolean(), any(FoodEnum.class), any(Pageable.class));
        verifyNoMoreInteractions(mockFoodRepository);
    }

    /**
     * Test of foodsIdGet method, of class FoodsApiController.
     */
    @Test
    public void testFoodsIdGet() throws Exception {
        System.out.println("foodsIdGet");

        given(mockFoodRepository.findById(10)).willReturn(mockFoodList.get(0));
        given(mockOrderItemRepository.findByFoodId(1L)).willReturn(mockOrderItemsList);
        System.out.println("mockFoodRepository.findById(10) --->" + mockFoodRepository.findById(10));
        // We perform the API call, and check that the response status code, and the JSON response are corrects
        mockMvc.perform(get("/api/foods/{id}?editable=true", 10l)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.foodItem.id", is(10)))
                .andExpect(jsonPath("$.foodItem.name", is("Pastitsio")))
                .andExpect(jsonPath("$.foodItem.description", is("test Pastitsio")))
                .andExpect(jsonPath("$.editable", is(false)));
    }

    /*
    *Test for 404 case, when the food's id does not exist 
     */
    @Test
    public void testFoodsIdGet_notFound_404() throws Exception {
        System.out.println("foodsIdGet NOT_FOUND");

        given(mockFoodRepository.findById(100)).willReturn(null);
        given(mockOrderItemRepository.findByFoodId(1L)).willReturn(null);
        System.out.println("mockFoodRepository.findById(100) --->" + mockFoodRepository.findById(100));
        // We perform the API call, and check that the response status code, and the JSON response are corrects
        mockMvc.perform(get("/api/foods/{id}?editable=true", 100l)).andExpect(status().isNotFound());

    }

    /*
    *Test for 400 case, when the editable boolean is setted from the user false
     */
    @Test
    public void testFoodsIdGet_badRequest_400_booleanFalse() throws Exception {
        System.out.println("foodsIdGet BAD_REQUEST");

        given(mockFoodRepository.findById(100)).willReturn(null);
        given(mockOrderItemRepository.findByFoodId(1L)).willReturn(null);

        // We perform the API call, and check that the response status code, and the JSON response are corrects
        mockMvc.perform(get("/api/foods/{id}?editable=false", 10l)).andExpect(status().isBadRequest());

    }

    /*
    *Test for 400 case, when the editable boolean is not setted from the user
     */
    @Test
    public void testFoodsIdGet_badRequest_400_booleanNull() throws Exception {
        System.out.println("foodsIdGet BAD_REQUEST");

        given(mockFoodRepository.findById(100)).willReturn(null);
        given(mockOrderItemRepository.findByFoodId(1L)).willReturn(null);

        // We perform the API call, and check that the response status code, and the JSON response are corrects
        mockMvc.perform(get("/api/foods/{id}", 10l)).andExpect(status().isBadRequest());

    }

    /**
     * Test of foodsFindByNameNameGet method, of class FoodsApiController. Case
     * food was found and returned.
     */
    @Test
    public void testFoodsFindByNameNameGet() throws Exception {

        // telling Mockito to use this mock food from mock list every time the findByName method is called on the foodRepo
        given(mockFoodRepository.findByName("Pastitsio")).willReturn(mockFoodList.get(0));

        // We perform the API call, and check that the response status code, and the JSON response are corrects
        mockMvc.perform(get("/api/foods/findByName/{name}", "Pastitsio")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(10)))
                .andExpect(jsonPath("$.name", is("Pastitsio")));

        // we verify that we called findByName method once only on the repo.
        verify(mockFoodRepository, times(1)).findByName("Pastitsio");
        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(mockFoodRepository);
    }

    /**
     * Test of foodsFindByNameNameGet method, of class FoodsApiController. Case
     * food not found
     */
    @Test
    public void testFoodsFindByNameNameGet_foodNotFound() throws Exception {

        given(mockFoodRepository.findByName("My Food")).willReturn(null);

        // We perform the API call, and check that the response status code, and the JSON response are corrects
        mockMvc.perform(get("/api/foods/findByName/{name}", "My Food"))
                .andExpect(status().isNotFound());

        verify(mockFoodRepository, times(1)).findByName("My Food");
        verifyNoMoreInteractions(mockFoodRepository);
    }

    /**
     * Test of foodsPost method, of class FoodsApiController. Case food created.
     */
//    @Ignore?
    @Test
    public void testFoodsPost() throws Exception {
        // telling Mockito to use this mock  list every time the findAll  method is called on the foodRepo
        given(mockFoodRepository.findAll()).willReturn(mockFoodList);
        // telling Mockito to use save the mockFood every time the save  method is called on the foodRepo
        given(mockFoodRepository.save(mockFood)).willReturn(new Food(1));
        // telling Mockito to return null every time the findByName  method is called on the foodRepo
        // *this method called from foods service 
        given(mockFoodRepository.findByName("Mousaka")).willReturn(null);

        /**
         * We perform the API call with body params, and check that the response
         * status code, and the JSON response are corrects
         */
        mockMvc.perform(post("/api/foods")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"foodType\": \"main\",\n"
                        + "  \"name\": \"Mousaka\",\n"
                        + "  \"description\":\"test Mousaka\",\n"
                        + "  \"price\": 5.00\n"
                        + "}"))
                .andExpect(status().isNoContent());

        // we verify that we called findAll method once only on the repo.
        verify(mockFoodRepository, times(1)).findByName("Mousaka");
        verify(mockFoodRepository, times(1)).save(mockFood);
        //in to pass this verification we need first to have mocked all the 
        // repo methods that called from controller or from service class
        verifyNoMoreInteractions(mockFoodRepository);
    }

    /**
     * Test of foodsPost method, of class FoodsApiController. Case bad request,
     * empty body params.
     */
    @Test
    public void testFoodsPost_400_badRequest_emptyFields() throws Exception {

        given(mockFoodRepository.findAll()).willReturn(mockFoodList);
        given(mockFoodRepository.save(any(Food.class))).willReturn(null);

        mockMvc.perform(post("/api/foods")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"foodType\": \"\",\n"
                        + "  \"name\": \"\",\n"
                        + "  \"description\":\"test Mousaka\",\n"
                        + "  \"price\":  \n"
                        + "}"))
                .andExpect(status().isBadRequest());

        verify(mockFoodRepository, times(0)).save(any(Food.class));
        verifyNoMoreInteractions(mockFoodRepository);
    }

    /**
     * Test of foodsPost method, of class FoodsApiController. Case bad request,
     * invalid food type param.
     */
    @Test
    public void testFoodsPost_400_badRequest_wrongFoodType() throws Exception {

        given(mockFoodRepository.findAll()).willReturn(mockFoodList);
        given(mockFoodRepository.save(any(Food.class))).willReturn(null);

        mockMvc.perform(post("/api/foods")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"foodType\": \" none\",\n"
                        + "  \"name\": \"Mousaka2\",\n"
                        + "  \"description\":\"test Mousaka\",\n"
                        + "  \"price\": 5.00\n"
                        + "}"))
                .andExpect(status().isBadRequest());

        verify(mockFoodRepository, times(0)).save(any(Food.class));
        verifyNoMoreInteractions(mockFoodRepository);
    }

    /**
     * Test of foodsPost method, of class FoodsApiController. Case bad request,
     * invalid price format .
     */
    @Test
    public void testFoodsPost_400_badRequest_wrongPrice() throws Exception {

        given(mockFoodRepository.findAll()).willReturn(mockFoodList);
        given(mockFoodRepository.save(any(Food.class))).willReturn(null);

        mockMvc.perform(post("/api/foods")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"foodType\": \" main\",\n"
                        + "  \"name\": \"Mousaka\",\n"
                        + "  \"description\":\"test Mousaka\",\n"
                        + "  \"price\": 12a\n"
                        + "}"))
                .andExpect(status().isBadRequest());

        verify(mockFoodRepository, times(0)).save(any(Food.class));
        // we verify that findByName called only once on the repo
        verify(mockFoodRepository, times(0)).findByName("Mousaka");
        verifyNoMoreInteractions(mockFoodRepository);
    }

    /**
     * Test of foodsPost method, of class FoodsApiController. Case preconditions
     * failed, food with the given param name already exists.
     */
    @Test
    public void testFoodsPost_412_foodExists() throws Exception {

        given(mockFoodRepository.findAll()).willReturn(mockFoodList);
        given(mockFoodRepository.save(any(Food.class))).willReturn(null);
        given(mockFoodRepository.findByName("Mousaka")).willReturn(mockFood);
        mockMvc.perform(post("/api/foods")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"foodType\": \"main\",\n"
                        + "  \"name\": \"Mousaka\",\n"
                        + "  \"description\":\"test Mousaka\",\n"
                        + "  \"price\": 5.00\n"
                        + "}"))
                .andExpect(status().isPreconditionFailed());

        // we verify that we called findAll method once only on the repo.
        verify(mockFoodRepository, times(0)).save(any(Food.class));
        // we verify that findByName called only once on the repo
        verify(mockFoodRepository, times(1)).findByName("Mousaka");
        verifyNoMoreInteractions(mockFoodRepository);
    }

    /**
     * Test of foodsIdDelete method, of class FoodsApiController. C
     */
    @Test
    public void testFoodsIdDelete() throws Exception {

    }

    /**
     * Test of foodsIdPut method, of class FoodsApiController. Case the food has
     * successfully updated/modified.
     */
    @Test
    public void testFoodsIdPut() throws Exception {

        given(mockFoodRepository.findOne(1l)).willReturn(mockFoodPut);
        given(orderItemRepository.findByFoodId(1l)).willReturn(new ArrayList<>());
        mockMvc.perform(put("/api/foods/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"FoodDetails\": {\n"
                        + "    \"foodType\": \"main\",\n"
                        + "    \"name\": \"new Name123\",\n"
                        + "    \"description\": \"new description\",\n"
                        + "    \"price\": 10.55\n"
                        + "  },\n"
                        + "  \"version\": 0\n"
                        + "}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.version", is(1)));

        // we verify that we called findAll method once only on the repo.
        verify(mockFoodRepository, times(1)).findOne(1l);
        verify(orderItemRepository, times(1)).findByFoodId(1l);
        verifyNoMoreInteractions(mockFoodRepository);
        verifyNoMoreInteractions(orderItemRepository);
    }

    /**
     * Test of foodsIdPut method, of class FoodsApiController. Case returned
     * 404, cause food not found.
     */
    @Test
    public void testFoodsIdPut_foodNotFound() throws Exception {

        given(mockFoodRepository.findOne(2l)).willReturn(null);
        mockMvc.perform(put("/api/foods/{id}", 2)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"FoodDetails\": {\n"
                        + "    \"foodType\": \"main\",\n"
                        + "    \"name\": \"new Name123\",\n"
                        + "    \"description\": \"new description\",\n"
                        + "    \"price\": 10.55\n"
                        + "  },\n"
                        + "  \"version\": 0\n"
                        + "}"))
                .andExpect(status().isNotFound());

        // we verify that we called findAll method once only on the repo.
        verify(mockFoodRepository, times(1)).findOne(2l);
        verifyNoMoreInteractions(mockFoodRepository);
    }

    /**
     * Test of foodsIdPut method, of class FoodsApiController. Case returned
     * 409, reason food version conflict.
     */
    @Test
    public void testFoodsIdPut_conflict() throws Exception {

        given(mockFoodRepository.findOne(1l)).willReturn(mockFoodPut);
        mockMvc.perform(put("/api/foods/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"FoodDetails\": {\n"
                        + "    \"foodType\": \"main\",\n"
                        + "    \"name\": \"new Name123\",\n"
                        + "    \"description\": \"new description\",\n"
                        + "    \"price\": 10.55\n"
                        + "  },\n"
                        + "  \"version\": 3\n"
                        + "}"))
                .andExpect(status().isConflict());

        // we verify that we called findAll method once only on the repo.
        verify(mockFoodRepository, times(1)).findOne(1l);
        verifyNoMoreInteractions(mockFoodRepository);
    }

    /**
     * Test of foodsIdPut method, of class FoodsApiController. Case returned
     * 400, reason food is archived .
     *
     */
    @Test
    public void testFoodsIdPut_badRequest() throws Exception {

        given(mockFoodRepository.findOne(10l)).willReturn(mockFoodList.get(0));
        mockMvc.perform(put("/api/foods/{id}", 10)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"FoodDetails\": {\n"
                        + "    \"foodType\": \"main\",\n"
                        + "    \"name\": \"new Name123\",\n"
                        + "    \"description\": \"new description\",\n"
                        + "    \"price\": 10.55\n"
                        + "  },\n"
                        + "  \"version\": 1\n"
                        + "}"))
                .andExpect(status().isBadRequest());

        // we verify that we called findAll method once only on the repo.
        verify(mockFoodRepository, times(1)).findOne(10l);
        verifyNoMoreInteractions(mockFoodRepository);
    }

    /**
     * Test of foodsIdPut method, of class FoodsApiController. Case returned
     * 409, reason food has not modified .
     *
     */
    @Test
    public void testFoodsIdPut_notModified() throws Exception {

        given(mockFoodRepository.findOne(10l)).willReturn(mockFoodPut);
        mockMvc.perform(put("/api/foods/{id}", 10)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"FoodDetails\": {\n"
                        + "    \"foodType\": \"main\",\n"
                        + "    \"name\": \"Pastitsio\",\n"
                        + "    \"description\": \"test pastitsio\",\n"
                        + "    \"price\": 5.00\n"
                        + "  },\n"
                        + "  \"version\": 1\n"
                        + "}"))
                .andExpect(status().isConflict());

        // we verify that we called findAll method once only on the repo.
        verify(mockFoodRepository, times(1)).findOne(10l);
        verifyNoMoreInteractions(mockFoodRepository);
    }

    /**
     * Test of foodsIdPut method, of class FoodsApiController. Case food has
     * been cloned.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testFoodsIdPut_foodCloned() throws Exception {

        given(mockFoodRepository.findOne(12l)).willReturn(mockFoodPut);
        given(orderItemRepository.findByFoodId(12l)).willReturn(new ArrayList<>());
        given(dailyMenuRepository.findByOfDateGreaterThanEqualAndFoodsId(LocalDate.now(), 12l)
        ).willReturn(new ArrayList<>());
        given(mockFoodRepository.save(any(Food.class))).willReturn(new Food());

        mockMvc.perform(put("/api/foods/{id}?clone=true", 12)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"FoodDetails\": {\n"
                        + "    \"foodType\": \"main\",\n"
                        + "    \"name\": \"Pastitsio1\",\n"
                        + "    \"description\": \"test pastitsio\",\n"
                        + "    \"price\": 5.00\n"
                        + "  },\n"
                        + "  \"version\": 0\n"
                        + "}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.version", is(0)));

        // we verify that we called findAll method once only on the repo.
        verify(mockFoodRepository, times(1)).findOne(12l);
        verify(mockFoodRepository, times(1)).save(any(Food.class));
        verifyNoMoreInteractions(mockFoodRepository);

    }
}
