/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;


import org.junit.Test;
import org.junit.Rule;
import org.junit.After;
import org.junit.Before;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.bootcamp.enums.FoodEnum;
import org.bootcamp.yum.data.entity.Yum;
import org.bootcamp.yum.data.entity.Food;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.service.YumService;
import org.bootcamp.yum.service.UserService;
import org.bootcamp.yum.data.entity.DailyMenu;
import org.bootcamp.yum.service.OrdersService;
import org.bootcamp.yum.data.entity.OrderItem;
import org.bootcamp.yum.data.entity.DailyOrder;
import org.bootcamp.yum.data.repository.YumRepository;
import org.bootcamp.yum.data.repository.FoodRepository;
import org.bootcamp.yum.data.repository.UserRepository;
import org.bootcamp.yum.data.repository.DailyMenuRepository;
import org.bootcamp.yum.data.repository.DailyOrderRepository;
import org.bootcamp.yum.data.repository.OrderItemRepository;
import org.bootcamp.yum.exception.ExceptionControllerAdvice;
import org.bootcamp.yum.service.SendMailService;
import org.bootcamp.test.annotation.WithMockAuth;
import org.bootcamp.test.MockAuthRule;

@RunWith(MockitoJUnitRunner.class)
public class OrdersApiControllerTest {

    @Rule
    public final MockAuthRule mockAuth = new MockAuthRule();

    @InjectMocks
    private final OrdersService orderService = new OrdersService();
    @Mock
    private final YumService yumService = new YumService();
    @Mock
    private final UserService userService = new UserService();
    @Mock
    private SendMailService mailService = new SendMailService();

    @Mock
    private DailyOrderRepository mockDailyOrderRepo;
    @Mock
    private FoodRepository mockFoodRepo;
    @Mock
    private DailyMenuRepository mockDailyMenuRepo;
    @Mock
    private OrderItemRepository mockOrderItemRepo;
    @Mock
    private UserRepository mockuserRepo;
    @Mock
    private YumRepository mockYumRepo;

    private MockMvc mockMvc;

    private static List<DailyOrder> mockDailyOrderList;
    private static List<User> mockUserList;
    private static List<Food> mockFoodList;
    private static List<DailyMenu> mockDailyMenuList;
    private static List<OrderItem> mockOrderItemList;
    private static List<Yum> mockYumList;
    private static DailyOrder mockDailyOrder;

    //
    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(new OrdersApiController(orderService))
                .setControllerAdvice(new ExceptionControllerAdvice())
                .build();
    }

    @BeforeClass
    public static void initMockData() {

        //instantiate lists
        mockDailyOrderList = new ArrayList<>();
        mockUserList = new ArrayList<>();
        mockFoodList = new ArrayList<>();
        mockDailyMenuList = new ArrayList<>();
        mockOrderItemList = new ArrayList<>();
        mockYumList = new ArrayList<>();

        //Create foods
        Food food1 = new Food(1);
        food1.setDescription("lalala");
        food1.setName("fakes");
        food1.setPrice(BigDecimal.valueOf(1000));
        food1.setType(FoodEnum.MAIN);
        Food food2 = new Food(2);
        food2.setDescription("lalala");
        food2.setName("arakas");
        food2.setPrice(BigDecimal.valueOf(1000));
        food2.setType(FoodEnum.MAIN);
        Food food3 = new Food(3);
        food3.setDescription("lalala");
        food3.setName("pastitsio");
        food3.setPrice(BigDecimal.valueOf(1000));
        food3.setType(FoodEnum.MAIN);
        mockFoodList.add(food1);
        mockFoodList.add(food2);
        mockFoodList.add(food3);

        //Create menus
        DailyMenu menu1 = new DailyMenu(1);
        menu1.setOfDate(LocalDate.parse("2017-05-05"));
        menu1.addFood(mockFoodList.get(0));
        menu1.addFood(mockFoodList.get(1));
        DailyMenu menu2 = new DailyMenu(2);
        menu2.setOfDate(LocalDate.parse("2017-06-10"));
        menu2.addFood(mockFoodList.get(0));
        menu2.addFood(mockFoodList.get(2));
        mockDailyMenuList.add(menu1);
        mockDailyMenuList.add(menu2);

        //Create users
        mockUserList.add(new User(1));
        mockUserList.add(new User(2));

        //Create orders
        DailyOrder order1 = new DailyOrder();
        order1.setId(1);
        order1.setDailyMenu(menu1);
        order1.setFinalOrder(false);
        order1.setVersion(1);
        order1.setUser(mockUserList.get(0));
        order1.addOrderItem(new OrderItem(mockFoodList.get(0), 2));
        order1.addOrderItem(new OrderItem(mockFoodList.get(1), 1));
        DailyOrder order2 = new DailyOrder();
        order2.setId(1);
        order2.setDailyMenu(menu2);
        order2.setFinalOrder(false);
        order2.setVersion(1);
        order2.setUser(mockUserList.get(1));
        order2.addOrderItem(new OrderItem(mockFoodList.get(0), 3));
        order2.addOrderItem(new OrderItem(mockFoodList.get(2), 4));
        mockDailyOrderList.add(order1);
        mockDailyOrderList.add(order2);
    }

    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of ordersIdDelete method, of class OrdersApiController.
     */
    @Test
    public void testOrdersIdDelete() {

    }

    /**
     * Test of ordersIdPut method, of class OrdersApiController.
     */
    @Test
    @WithMockAuth(id = "2")
    public void testOrdersIdPutOk() throws Exception {
        given(mockDailyOrderRepo.findOne(2L)).willReturn(mockDailyOrderList.get(1));
        given(yumService.getDeadline()).willReturn(org.joda.time.LocalTime.MIDNIGHT.minusHours(12));
        given(userService.getLoggedInUser()).willReturn(mockUserList.get(1));
        given(mockFoodRepo.findById(3L)).willReturn(mockFoodList.get(2));
        // We perform the API call, and check that the response status code, and the JSON response are corrects
        mockMvc.perform(put("/api/orders/2")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"version\": 1,\n"
                        + "  \"sendMail\": \"true\",\n"
                        + "  \"date\": \"2017-05-17\",\n"
                        + "  \"menuVersion\": 0,\n"
                        + "  \"menuId\": 1,"
                        + "  \"orderItems\": [\n"
                        + "{\"foodId\":3,\n"
                        + "\"quantity\":1\n"
                        + "}\n"
                        + "]\n"
                        + "}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    @WithMockAuth(id = "1")
    public void testOrdersIdPutAccessDenied() throws Exception {
        given(mockDailyOrderRepo.findOne(2L)).willReturn(mockDailyOrderList.get(1));
        given(yumService.getDeadline()).willReturn(org.joda.time.LocalTime.MIDNIGHT.minusHours(12));
        given(userService.getLoggedInUser()).willReturn(mockUserList.get(0));
        given(mockFoodRepo.findById(3L)).willReturn(mockFoodList.get(2));
        // We perform the API call, and check that the response status code, and the JSON response are corrects
        mockMvc.perform(put("/api/orders/2")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"version\": 1,\n"
                        + "  \"sendMail\": \"true\",\n"
                        + "  \"date\": \"2017-05-17\",\n"
                        + "  \"menuVersion\": 0,\n"
                        + "  \"menuId\": 1,"
                        + "  \"orderItems\": [\n"
                        + "     {\"foodId\":3,\n"
                        + "      \"quantity\":1\n"
                        + "     }\n"
                        + "   ]\n"
                        + "}"))
                .andExpect(status().is(403))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    /**
     * Test of ordersIdGet method, of class OrdersApiController.
     */
    @Test
    @WithMockAuth(id = "2")
    public void testOrdersIdGetOK() throws Exception {
        given(mockDailyOrderRepo.findOne(2L)).willReturn(mockDailyOrderList.get(1));
        given(yumService.getDeadline()).willReturn(org.joda.time.LocalTime.MIDNIGHT.minusHours(12));
        given(userService.getLoggedInUser()).willReturn(mockUserList.get(1));
        mockMvc.perform(get("/api/orders/2?id=1&menuId=1&menuVersion=1&date=2017-05-05")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

    }

    /**
     * Test of ordersPost method, of class OrdersApiController.
     */
    @Test
    @WithMockAuth(id = "1")
    public void testOrdersPostOk() throws Exception {
        given(userService.getLoggedInUser()).willReturn(mockUserList.get(0));
        given(mockDailyMenuRepo.findByOfDate(any(LocalDate.class))).willReturn(mockDailyMenuList.get(1));
        given(mockDailyMenuRepo.findById(any(Long.class))).willReturn(mockDailyMenuList.get(1));
        given(yumService.getDeadline()).willReturn(org.joda.time.LocalTime.MIDNIGHT.minusHours(12));
        given(mockDailyOrderRepo.findByUserIdAndDailyMenuId(1L, 2L)).willReturn(null);
        given(mockFoodRepo.findById(1L)).willReturn(mockFoodList.get(0));

        // We perform the API call, and check that the response status code, and the JSON response are corrects
        mockMvc.perform(post("/api/orders")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"date\": \"2017-06-10\",\n"
                        + "  \"menuVersion\": 0,\n"
                        + "  \"menuId\": 2,"
                        + "  \"sendMail\": \"true\",\n"
                        + "  \"orderItems\": [\n"
                        + "{\"foodId\":1,\n"
                        + "\"quantity\":1\n"
                        + "}\n"
                        + "]\n"
                        + "}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

    }

    @Test
    @WithMockAuth(id = "1")
    public void testOrdersPostBadRequest() throws Exception {
        given(userService.getLoggedInUser()).willReturn(mockUserList.get(0));
        given(mockDailyMenuRepo.findByOfDate(LocalDate.parse("2017-05-10"))).willReturn(mockDailyMenuList.get(1));
        given(yumService.getDeadline()).willReturn(org.joda.time.LocalTime.MIDNIGHT.minusHours(12));
        given(mockDailyOrderRepo.findByUserIdAndDailyMenuId(1L, 2L)).willReturn(null);
        given(mockFoodRepo.findById(3L)).willReturn(null);
        given(yumService.getDeadline()).willReturn(org.joda.time.LocalTime.MIDNIGHT.minusHours(12));

        // We perform the API call, and check that the response status code, and the JSON response are corrects
        mockMvc.perform(post("/api/orders")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"date\": \"2017-05-10\",\n"
                        + "  \"sendMail\": \"true\",\n"
                        + "  \"orderItems\": [\n"
                        + "{\"foodId\":3,\n"
                        + "\"quantity\":1\n"
                        + "}\n"
                        + "]\n"
                        + "}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

    }

    /**
     * Test of ordersDailyDayGet method, of class OrdersApiController.
     */
    @Test
    public void testOrdersDailyDayGet() {

    }

    /**
     * Test of ordersMonthlyGet method, of class OrdersApiController.
     */
    @Test
    public void testOrdersMonthlyGet() {

    }

    /**
     * Test of ordersMonthlyMonthYearGet method, of class OrdersApiController.
     */
    @Test
    public void testOrdersMonthlyMonthYearGet() {

    }

}
