/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;

import java.util.ArrayList;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import org.junit.Test;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.BDDMockito.given;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.bootcamp.enums.UserRoleEnum;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.service.AuthService;
import org.bootcamp.yum.data.repository.UserRepository;
import org.bootcamp.yum.exception.ExceptionControllerAdvice;
import org.bootcamp.yum.service.SendMailService;

@RunWith(MockitoJUnitRunner.class)
public class AuthApiControllerTest {

    @InjectMocks
    private final AuthService authService = new AuthService();

    @Mock
    private UserRepository mockUserRepository;

    @Mock
    private SendMailService mailService;

    private MockMvc mockMvc;

    private static User mockUser;
    private static User mockUserChangepwd;
    private static User mockUserExpiredSecret;
    private static ArrayList<User> mockUsersList;

    @BeforeClass
    public static void setUpClass() {
        mockUsersList = new ArrayList<>();
        String hashedPassword = BCrypt.hashpw("123456", BCrypt.gensalt());
        mockUser = new User(1);
        mockUser.setEmail("bw.bat@mail.com");
        mockUser.setPassword(hashedPassword);
        mockUser.setApproved(true);
        mockUser.setFirstName("bruce");
        mockUser.setLastName("wayne");
        mockUser.setRegistrationDate(LocalDate.parse("2017-12-12"));
        mockUser.setRole(UserRoleEnum.HUNGRY);
        mockUser.setVersion(1);
        mockUsersList.add(mockUser);

        mockUserChangepwd = new User(2);
        mockUserChangepwd.setEmail("bw.bat@mail.com");
        mockUserChangepwd.setPassword(hashedPassword);
        mockUserChangepwd.setApproved(true);
        mockUserChangepwd.setFirstName("bruce");
        mockUserChangepwd.setLastName("wayne");
        mockUserChangepwd.setRegistrationDate(LocalDate.parse("2017-12-12"));
        mockUserChangepwd.setRole(UserRoleEnum.HUNGRY);
        mockUserChangepwd.setVersion(1);
        mockUserChangepwd.setSecret("$2a$10$67et.utxkevLGcycqLmXCOKQEw.1ja33hKMjYWTVCJmqyb583IHci");
        mockUserChangepwd.setSecretCreation(LocalDateTime.now());

        mockUserExpiredSecret = new User(3);
        mockUserExpiredSecret.setEmail("bw.bat@mail.com");
        mockUserExpiredSecret.setPassword(hashedPassword);
        mockUserExpiredSecret.setApproved(true);
        mockUserExpiredSecret.setFirstName("bruce");
        mockUserExpiredSecret.setLastName("wayne");
        mockUserExpiredSecret.setRegistrationDate(LocalDate.parse("2017-12-12"));
        mockUserExpiredSecret.setRole(UserRoleEnum.HUNGRY);
        mockUserExpiredSecret.setVersion(1);
        mockUserExpiredSecret.setSecret("$2a$10$67et.utxkevLGcycqLmXCOKQEw.1ja33hKMjYWTVCJmqyb583IHci");
        mockUserExpiredSecret.setSecretCreation(LocalDateTime.now().plusDays(5));
    }

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(new AuthApiController(authService))
                .setControllerAdvice(new ExceptionControllerAdvice())
                .build();
    }

    /**
     * Test of authForgotpwdPost method, of class AuthApiController. Case the
     * e-mail has successfully sent to user.
     */
    @Test
    public void testAuthForgotpwdPost_200_ok() throws Exception {

        given(mockUserRepository.findByEmail("bw.bat@mail.com")).willReturn(mockUser);
        mockMvc.perform(post("/api/auth/forgotpwd")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"email\": \"bw.bat@mail.com\"\n"
                        + "}"))
                .andExpect(status().isNoContent());

        verify(mockUserRepository, times(1)).findByEmail("bw.bat@mail.com");
        verifyNoMoreInteractions(mockUserRepository);
    }

    /**
     * Test of authForgotpwdPost method, of class AuthApiController. Case
     * invalid e-mail format (body params)
     */
    @Test
    public void testAuthForgotpwdPost_400_invalidEmailFormat() throws Exception {
        given(mockUserRepository.findByEmail("bw.batmail.com")).willReturn(null);

        mockMvc.perform(post("/api/auth/forgotpwd")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"email\": \"bw.batmail.com\"\n"
                        + "}"))
                .andExpect(status().isBadRequest());

        verify(mockUserRepository, times(0)).findByEmail("bw.batmail.com");
        verifyNoMoreInteractions(mockUserRepository);
    }

    /**
     * Test of authForgotpwdPost method, of class AuthApiController. Case e-mail
     * not found in db.
     */
    @Test
    public void testAuthForgotpwdPost_404_emailNotFound() throws Exception {
        given(mockUserRepository.findByEmail("bw.bat@mail.com")).willReturn(null);

        mockMvc.perform(post("/api/auth/forgotpwd")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"email\": \"bw.bat@mail.com\"\n"
                        + "}"))
                .andExpect(status().isNotFound());

        verify(mockUserRepository, times(1)).findByEmail("bw.bat@mail.com");
        verifyNoMoreInteractions(mockUserRepository);
    }

    /**
     * Test of authLoginPost method, of class AuthApiController. Case user
     * successfully logged in.
     */
    @Test
    public void testAuthLoginPost() throws Exception {

        given(mockUserRepository.findByEmail("bw.bat@mail.com")).willReturn(mockUser);

        mockMvc.perform(post("/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"email\": \"bw.bat@mail.com\",\n"
                        + "  \"password\": \"123456\"\n"
                        + "}"))
                .andExpect(status().isOk());

        verify(mockUserRepository, times(1)).findByEmail("bw.bat@mail.com");
        verifyNoMoreInteractions(mockUserRepository);
    }

    /**
     * Test of authLoginPost method, of class AuthApiController. Case bad
     * credentials (invalid email format)
     */
    @Test
    public void testAuthLoginPost_403_inValidEmainFormat() throws Exception {

        given(mockUserRepository.findByEmail("bw.bat@mail.com")).willReturn(null);

        mockMvc.perform(post("/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"email\": \"bw.bat.mail.com\",\n"
                        + "  \"password\": \"123456\"\n"
                        + "}"))
                .andExpect(status().isBadRequest());

        verify(mockUserRepository, times(0)).findByEmail("bw.bat@mail.com");
        verifyNoMoreInteractions(mockUserRepository);
    }

    /**
     * Test of authLoginPost method, of class AuthApiController. Case bad
     * credentials (invalid password length)
     */
    @Test

    public void testAuthLoginPost_403_inValidPasswordLength() throws Exception {

        given(mockUserRepository.findByEmail("bw.bat@mail.com")).willReturn(null);

        mockMvc.perform(post("/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"email\": \"bw.bat.mail.com\",\n"
                        + "  \"password\": \"1234\"\n"
                        + "}"))
                .andExpect(status().isBadRequest());

        verify(mockUserRepository, times(0)).findByEmail("bw.bat@mail.com");
        verifyNoMoreInteractions(mockUserRepository);
    }

    /**
     * Test of authLoginPost method, of class AuthApiController. Case bad
     * credentials (wrong password )
     */
    @Test
    public void testAuthLoginPost_403_inValidPassword() throws Exception {

        given(mockUserRepository.findByEmail("bw.bat@mail.com")).willReturn(null);

        mockMvc.perform(post("/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"email\": \"bw.bat@mail.com\",\n"
                        + "  \"password\": \"1234654\"\n"
                        + "}"))
                .andExpect(status().isForbidden());

        verify(mockUserRepository, times(1)).findByEmail("bw.bat@mail.com");
        verifyNoMoreInteractions(mockUserRepository);
    }

    /**
     * Test of authLoginPost method, of class AuthApiController. Case bad
     * credentials (wrong password )
     */
    @Test
    public void testAuthLoginPost_403_inValidEmail() throws Exception {

        given(mockUserRepository.findByEmail("test@mail.com")).willReturn(null);

        mockMvc.perform(post("/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"email\": \"test@mail.com\",\n"
                        + "  \"password\": \"123465\"\n"
                        + "}"))
                .andExpect(status().isForbidden());

        verify(mockUserRepository, times(1)).findByEmail("test@mail.com");
        verifyNoMoreInteractions(mockUserRepository);
    }

    /**
     * Test of authLoginPost method, of class AuthApiController. Case empty
     * request
     */
    @Test
    public void testAuthLoginPost_500_emptyRequest() throws Exception {

        given(mockUserRepository.findByEmail("test@mail.com")).willReturn(null);

        mockMvc.perform(post("/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{}"))
                .andExpect(status().isInternalServerError());

        verify(mockUserRepository, times(0)).findByEmail("test@mail.com");
        verifyNoMoreInteractions(mockUserRepository);
    }

    /**
     * Test of authRegisterPost method, of class AuthApiController. Case user
     * has successfully registered.
     */
    @Test
    public void testAuthRegisterPost() throws Exception {

        given(mockUserRepository.findByEmail("bw.bat@mail.com")).willReturn(null);
        given(mockUserRepository.save(any(User.class))).willReturn(new User(1));

        mockMvc.perform(post("/api/auth/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"firstName\": \"bruce\",\n"
                        + "  \"lastName\": \"wayne\",\n"
                        + "  \"email\": \"bw.bat@mail.com\",\n"
                        + "  \"password\": \"123456\",\n"
                        + "  \"version\": 0\n"
                        + "}"))
                .andExpect(status().isNoContent());

        verify(mockUserRepository, times(1)).findByEmail("bw.bat@mail.com");
        verify(mockUserRepository, times(1)).save(any(User.class));
        verifyNoMoreInteractions(mockUserRepository);
    }

    /**
     * Test of authRegisterPost method, of class AuthApiController. Case user
     * (e-mail) already exists.
     *
     */
    @Test
    public void testAuthRegisterPost_userExists() throws Exception {

        given(mockUserRepository.findByEmail("bw.bat@mail.com")).willReturn(mockUser);
        given(mockUserRepository.save(any(User.class))).willReturn(null);

        mockMvc.perform(post("/api/auth/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"firstName\": \"bruce\",\n"
                        + "  \"lastName\": \"wayne\",\n"
                        + "  \"email\": \"bw.bat@mail.com\",\n"
                        + "  \"password\": \"123456\",\n"
                        + "  \"version\": 0\n"
                        + "}"))
                .andExpect(status().isForbidden());

        verify(mockUserRepository, times(1)).findByEmail("bw.bat@mail.com");
        verify(mockUserRepository, times(0)).save(any(User.class));
        verifyNoMoreInteractions(mockUserRepository);
    }

    /**
     * Test of authRegisterPost method, of class AuthApiController. Case empty
     * params
     */
    @Test
    public void testAuthRegisterPost_EmptyParams() throws Exception {

        given(mockUserRepository.findByEmail("bw.bat@mail.com")).willReturn(mockUser);
        given(mockUserRepository.save(any(User.class))).willReturn(new User(1));

        mockMvc.perform(post("/api/auth/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"firstName\": \"\",\n"
                        + "  \"lastName\": \"\",\n"
                        + "  \"email\": \"\",\n"
                        + "  \"password\": \"\",\n"
                        + "  \"version\": 0\n"
                        + "}"))
                .andExpect(status().isBadRequest());

        verify(mockUserRepository, times(0)).findByEmail("bw.bat@mail.com");
        verify(mockUserRepository, times(0)).save(any(User.class));
        verifyNoMoreInteractions(mockUserRepository);
    }

    /**
     * Test of authRegisterPost method, of class AuthApiController. Case invalid
     * e-mail format
     */
    @Test
    public void testAuthRegisterPost_inValidEmail() throws Exception {

        given(mockUserRepository.findByEmail("bw.bat.mail.com")).willReturn(null);
        given(mockUserRepository.save(any(User.class))).willReturn(null);

        mockMvc.perform(post("/api/auth/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"firstName\": \"bruce\",\n"
                        + "  \"lastName\": \"wayne\",\n"
                        + "  \"email\": \"bw.bat.mail.com\",\n"
                        + "  \"password\": \"123456\",\n"
                        + "  \"version\": 0\n"
                        + "}"))
                .andExpect(status().isBadRequest());

        verify(mockUserRepository, times(0)).findByEmail("bw.bat.mail.com");
        verify(mockUserRepository, times(0)).save(any(User.class));
        verifyNoMoreInteractions(mockUserRepository);
    }

    /**
     * Test of authRegisterPost method, of class AuthApiController. Case invalid
     * password length
     */
    @Test
    public void testAuthRegisterPost_inValidPassWordLength() throws Exception {

        given(mockUserRepository.findByEmail("bw.bat@mail.com")).willReturn(null);
        given(mockUserRepository.save(any(User.class))).willReturn(null);

        mockMvc.perform(post("/api/auth/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"firstName\": \"bruce\",\n"
                        + "  \"lastName\": \"wayne\",\n"
                        + "  \"email\": \"bw.bat@mail.com\",\n"
                        + "  \"password\": \"123\",\n"
                        + "  \"version\": 0\n"
                        + "}"))
                .andExpect(status().isBadRequest());

        verify(mockUserRepository, times(0)).findByEmail("bw.bat@mail.com");
        verify(mockUserRepository, times(0)).save(any(User.class));
        verifyNoMoreInteractions(mockUserRepository);
    }

    /**
     * Test of authRegisterPost method, of class AuthApiController. Case empty
     * request body params.
     */
    @Test
    public void testAuthRegisterPost_emptyRequestBodyParams() throws Exception {

        given(mockUserRepository.findByEmail("")).willReturn(null);
        given(mockUserRepository.save(any(User.class))).willReturn(null);

        mockMvc.perform(post("/api/auth/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{}"))
                .andExpect(status().isBadRequest());

        verify(mockUserRepository, times(0)).findByEmail("");
        verify(mockUserRepository, times(0)).save(any(User.class));
        verifyNoMoreInteractions(mockUserRepository);
    }

    /**
     * Test of authChangepwdPut method, of class AuthApiController. Case user
     * has successfully change his password.
     */
    @Test
    public void testAuthChangepwdPut() throws Exception {

        given(mockUserRepository.findBySecret(any(String.class))).willReturn(mockUserChangepwd);

        mockMvc.perform(put("/api/auth/changepwd")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"secret\": \"$2a$10$67et.utxkevLGcycqLmXCOKQEw.1ja33hKMjYWTVCJmqyb583IHci\",\n"
                        + "  \"password\": \"string\"\n"
                        + "}"))
                .andExpect(status().isNoContent());

        verify(mockUserRepository, times(1)).findBySecret(any(String.class));
        verifyNoMoreInteractions(mockUserRepository);
    }

    /**
     * Test of authChangepwdPut method, of class AuthApiController. Case secret
     * has expired.
     */
    @Test
    public void testAuthChangepwdPut_expiredSecret() throws Exception {

        given(mockUserRepository.findBySecret(any(String.class))).willReturn(mockUserExpiredSecret);

        mockMvc.perform(put("/api/auth/changepwd")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"secret\": \"$2a$10$67et.utxkevLGcycqLmXCOKQEw.1ja33hKMjYWTVCJmqyb583IHci\",\n"
                        + "  \"password\": \"string\"\n"
                        + "}"))
                .andExpect(status().isNoContent());

        verify(mockUserRepository, times(1)).findBySecret(any(String.class));
        verifyNoMoreInteractions(mockUserRepository);
    }

    /**
     * Test of authChangepwdPut method, of class AuthApiController. Case invalid
     * secret.
     */
    @Test
    public void testAuthChangepwdPut_invalidSecret() throws Exception {

        given(mockUserRepository.findBySecret(any(String.class))).willReturn(null);

        mockMvc.perform(put("/api/auth/changepwd")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"secret\": \"$2a$10$67et.utxkevLGcycqLmXCOKQ\",\n"
                        + "  \"password\": \"string\"\n"
                        + "}"))
                .andExpect(status().isBadRequest());

        verify(mockUserRepository, times(1)).findBySecret(any(String.class));
        verifyNoMoreInteractions(mockUserRepository);
    }

    /**
     * Test of authChangepwdPut method, of class AuthApiController. Case empty
     * request.
     */
    @Test
    public void testAuthChangepwdPut_emptyRequest() throws Exception {

        given(mockUserRepository.findBySecret(any(String.class))).willReturn(null);

        mockMvc.perform(put("/api/auth/changepwd")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{}"))
                .andExpect(status().isBadRequest());

        verify(mockUserRepository, times(0)).findBySecret(any(String.class));
        verifyNoMoreInteractions(mockUserRepository);
    }

    /**
     * Test of authChangepwdPut method, of class AuthApiController. Case invalid
     * password length
     */
    @Test
    public void testAuthChangepwdPut_invalidPasswordLength() throws Exception {

        given(mockUserRepository.findBySecret(any(String.class))).willReturn(null);

        mockMvc.perform(put("/api/auth/changepwd")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"secret\": \"$2a$10$67et.utxkevLGcycqLmXCOKQEw.1ja33hKMjYWTVCJmqyb583IHci\",\n"
                        + "  \"password\": \"123\"\n"
                        + "}"))
                .andExpect(status().isBadRequest());

        verify(mockUserRepository, times(0)).findBySecret(any(String.class));
        verifyNoMoreInteractions(mockUserRepository);
    }

}
