import { Injectable } from '@angular/core';
import {
  CanActivateChild,
  CanActivate,
  Router
} from '@angular/router';

import { AuthenticationService } from './shared/authentication.service';

@Injectable()
export class AppGuard implements CanActivate {

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) { }

  canActivate(): boolean {
    var userRole = this.authenticationService.getLoggedInRole();
    if (userRole) {
      this.router.navigate(['/' + userRole]);
      return false;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}

@Injectable()
export class IsLoggedInGuard implements CanActivateChild {

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) { }

  canActivateChild(): boolean {
    let logged = this.authenticationService.isLogged();
    if (logged) return true;

    this.router.navigate(['/']);
    return false;
  }
}

@Injectable()
export class CanLoginGuard implements CanActivate {

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) { }

  canActivate(): boolean {

    if (this.authenticationService.isLogged()) {
      this.router.navigate(['/']);
      return false;
    } else {
      return true;
    }
  }

}

@Injectable()
export class CanActivateChefGuard implements CanActivateChild {

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) { }

  canActivateChild(): boolean {
    var userRole = this.authenticationService.getLoggedInRole();

    if (userRole === 'admin' || userRole === 'chef') return true;
    // otherwise
    this.router.navigate(['/']);
    return false;
  }
}

@Injectable()
export class CanActivateAdminGuard implements CanActivateChild {

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) { }

  canActivateChild(): boolean {
    var userRole = this.authenticationService.getLoggedInRole();
    if (userRole === 'admin') return true;
    // otherwise
    this.router.navigate(['/']);
    return false;
  }
}


@Injectable()
export class CanActivateSettingsGuard implements CanActivate {

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) { }

  canActivate(): boolean {

    if (this.authenticationService.isLogged()) {
      return true;
    } else {
      this.router.navigate(['/']);
      return false;
    }
  }

}
