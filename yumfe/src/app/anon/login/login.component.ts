import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MdSnackBar } from '@angular/material';

import { AuthenticationService } from '../../shared/authentication.service';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public loading = false;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    public snackBar: MdSnackBar
  ) { }

  ngOnInit() {

    this.loginForm = new FormGroup({
      user: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(25)
      ]))

    });

  }

  login() {

    this.loading = true;

    this.authenticationService.login(this.loginForm.get('user').value, this.loginForm.get('password').value)
      .subscribe(result => {
        if (result === true) {
          this.router.navigate(['/']);
        } else {
          // login failed
          this.openErrorSnackBar('Username or password is incorrect', 'ok');
          this.loading = false;
        }
      }, error => {
        this.loading = false;

        if (error.error === '403') {
          this.openErrorSnackBar(error.message, 'ok');
        } else {
          this.openErrorSnackBar('Username or password is incorrect', 'ok');
        }
      });

  }

  gotoForgotpwd() {

    this.router.navigate(['/forgotpwd']);

  }

  gotoRegister() {

    this.router.navigate(['/register']);

  }

  // Opens an Error SnackBar with the requested message and action
  openErrorSnackBar(message: string, action: string): void {

    this.snackBar.open(message, action, {
      extraClasses: ['error-snack-bar']
    });

  }

}
