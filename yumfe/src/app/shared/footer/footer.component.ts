import { Component, OnInit } from '@angular/core';

import { TosService } from '../tos.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor(public tosService: TosService) { }

  ngOnInit() {
  }

  // if flag === true focus tab on
  //terms, else on policy
  displayTermsAndPolicy(flag) {
    this.tosService.displayTos(flag);
  }

}
