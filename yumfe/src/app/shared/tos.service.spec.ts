import { TestBed, inject } from '@angular/core/testing';

import { TosService } from './tos.service';

describe('TosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TosService]
    });
  });

  it('should ...', inject([TosService], (service: TosService) => {
    expect(service).toBeTruthy();
  }));
});
