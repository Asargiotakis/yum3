import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-monthly-pagination',
  templateUrl: './monthly-pagination.component.html',
  styleUrls: ['./monthly-pagination.component.scss']
})
export class MonthlyPaginationComponent implements OnInit {

  @Input()
  private basePath: string;
  @Input()
  private month: number;
  @Input()
  private year: number;
  @Input()
  public viewMonth: Date;
  public title: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {

    this.route.url.subscribe(urlSegments => {
      switch (urlSegments[0].path) {
        case 'history':
          this.title = 'My orders\' history';
          break;
        case 'menus':
          this.title = 'Assign foods to dates...';
          break;
        case 'orders':
          this.title = 'Your orders';
          break;
        default:
          this.title = '';
          break;
      }
    });

  }

  // Navigates to another month
  changeMonth(value: number): void {

    let monthToRequest: String;
    let yearToRequest: number;

    if (value === -1) {
      // Navigate to previous month
      if (this.month === 0) {
        monthToRequest = '12';
        yearToRequest = this.year - 1;
        this.router.navigate([this.basePath, yearToRequest, monthToRequest]);
      } else if (this.month < 10) {
        monthToRequest = '0' + this.month;
        yearToRequest = this.year;
        this.router.navigate([this.basePath, yearToRequest, monthToRequest]);
      } else {
        monthToRequest = '' + this.month;
        yearToRequest = this.year;
        this.router.navigate([this.basePath, yearToRequest, monthToRequest]);
      }

    } else if (value === 1) {
      // Navigate to next month
      if (this.month === 11) {
        monthToRequest = '01';
        yearToRequest = this.year + 1;
        this.router.navigate([this.basePath, yearToRequest, monthToRequest]);
      } else if (this.month < 8) {
        monthToRequest = '0' + (this.month + 2);
        yearToRequest = this.year;
        this.router.navigate([this.basePath, yearToRequest, monthToRequest]);
      } else {
        monthToRequest = '' + (this.month + 2);
        yearToRequest = this.year;
        this.router.navigate([this.basePath, yearToRequest, monthToRequest]);
      }

    } else {
      // Wrong parameter - redirect to current month
      this.router.navigate([this.basePath]);
    }

  }

}
