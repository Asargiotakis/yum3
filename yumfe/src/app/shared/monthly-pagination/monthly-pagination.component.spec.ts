import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyPaginationComponent } from './monthly-pagination.component';

describe('MonthlyPaginationComponent', () => {
  let component: MonthlyPaginationComponent;
  let fixture: ComponentFixture<MonthlyPaginationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonthlyPaginationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyPaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
