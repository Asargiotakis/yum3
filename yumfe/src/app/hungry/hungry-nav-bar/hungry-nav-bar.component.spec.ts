import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HungryNavBarComponent } from './hungry-nav-bar.component';

describe('HungryNavBarComponent', () => {
  let component: HungryNavBarComponent;
  let fixture: ComponentFixture<HungryNavBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HungryNavBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HungryNavBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
