import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MdDialog, MdDialogRef, MdSnackBar } from '@angular/material';

import * as remote from '../../../remote';
import { Foods } from './foods';
import { CustomIcons } from '../../../shared/custom-icons/custom-icons';
import { NumberValidator } from './number.validator';
import { AuthenticationService } from '../../../shared/authentication.service';
import { GlobalSettingsService } from '../../../shared/globalSettings.service';

@Component({
  selector: 'app-hungry-daily-order',
  templateUrl: './daily-order.component.html',
  styleUrls: ['./daily-order.component.scss']
})
export class DailyOrderComponent implements OnInit {

  @Input()
  public dailyMenu: remote.DailyMenu;
  @Output()
  public dailyTotal = new EventEmitter();
  public dailyOrderForm: FormGroup = new FormGroup({});
  public foods = new Map<string, Foods>();
  public total: number = 0;
  public isFinal: boolean = false;
  public hasOrdered: boolean = false;
  public orderButtonText: string = 'Order';
  public editMode: boolean = false;
  public buttonDisable: boolean;
  public customIcons: CustomIcons = new CustomIcons();

  constructor(
    private hungryService: remote.HungryApi,
    private authenticationService: AuthenticationService,
    public globalSettingsService: GlobalSettingsService,
    private dialog: MdDialog,
    public snackBar: MdSnackBar
  ) {
    // Prepare foods map by type
    this.foods.set('main', new Foods());
    this.foods.set('salad', new Foods());
    this.foods.set('drink', new Foods());
    this.foods.set('all', new Foods());
  }

  ngOnInit() {

    if (this.dailyMenu !== undefined) {
      // Process Daily Menu
      this.dailyMenuProcessor();
    }

  }

  // Daily Menu processor
  dailyMenuProcessor(): void {

    // Check if dailyMenu is final
    if (this.dailyMenu.final) {
      this.isFinal = true;
    }
    // Check if user has made an order
    if (this.dailyMenu.orderId > -1) {
      this.hasOrdered = true;
      this.orderButtonText = 'Save';
    } else {
      this.hasOrdered = false;
      this.orderButtonText = 'Order';
    }
    // Check if the form can appear in edit mode
    if (!this.isFinal && !this.hasOrdered) {
      this.editMode = true;
    } else {
      this.editMode = false;
    }
    // Process foods
    this.foodsProcessor();

    // Update dailyOrderForm edit status
    if (!this.editMode) {
      this.dailyOrderForm.disable();
    } else {
      this.dailyOrderForm.enable();
    }

    // Update total price
    this.dailyOrderForm.valueChanges.subscribe(value => {
      this.total = 0;
      this.dailyOrderForm.markAsPristine();
      for (let prop in value) {
        if (value.hasOwnProperty(prop)) {
          if (this.foods.get('all').getFood(+prop).quantity !== value[prop]) {
            this.dailyOrderForm.markAsDirty();
          }
          if (value[prop] > 0) {
            this.total += this.foods.get('all').getFood(+prop).price * value[prop];
          }
        }
      }
    });

    // Emit dailyTotal
    this.dailyTotal.emit({
      date: this.dailyMenu.date,
      total: this.total
    });

    // Mark the Form as untouched
    this.dailyOrderForm.markAsUntouched();

  }

  // Distinguishes foods by type and updates the foods map
  foodsProcessor(): void {

    // Remove all Form Controls
    this.dailyOrderForm.controls = {};

    // Remove all existing foods from the map
    this.foods.get('main').removeAll();
    this.foods.get('salad').removeAll();
    this.foods.get('drink').removeAll();
    this.foods.get('all').removeAll();

    // Populate foods map with the new foods
    for (let food of this.dailyMenu.foods) {
      // Insert the food in the generic type
      this.foods.get('all').addFood(food);
      // Categorize the foods by type
      switch (food.foodType) {
        case 'MAIN':
          this.foods.get('main').addFood(food);
          break;
        case 'SALAD':
          this.foods.get('salad').addFood(food);
          break;
        case 'DRINK':
          this.foods.get('drink').addFood(food);
          break;
        default:
          break;
      }
      // Update total price
      this.total += food.quantity * food.price;

      // Add a FormControl for the current food
      this.dailyOrderForm.addControl('' + food.id,
        new FormControl(this.hasOrdered ? food.quantity : 0,
          Validators.compose([
            Validators.required,
            NumberValidator.isInteger
          ])));
    }

  }

  // Handles which order method to call
  placeOrder(sendEmailPrefer: boolean) {

    if (this.hasOrdered) {
      this.putOrder(sendEmailPrefer);
    } else {
      this.postOrder(sendEmailPrefer);
    }

  }

  // API call for placing a new order
  postOrder(sendEmailPrefer: boolean): void {

    if (this.total > 0) {
      // Updated buttonDisable
      this.buttonDisable = true;

      // Construct order items
      let orderItemsArray: Array<remote.OrderItem> = this.generateOrderItemsArray();

      // Construct request DTO
      let orderDetails: remote.OrderDetails = {
        date: this.dailyMenu.date,
        menuVersion: this.dailyMenu.version,
        menuId: this.dailyMenu.id,
        sendMail: sendEmailPrefer,
        orderItems: orderItemsArray
      };

      // Make call to ordersPost API
      this.hungryService.ordersPost(orderDetails).subscribe(dailymenu => {
        this.dailyMenu = dailymenu;
        // Process Daily Menu
        this.dailyMenuProcessor();
        // Update buttonDisable
        this.buttonDisable = false;
        // Display SnackBar
        this.openSuccessSnackBar('Order placed!', 'ok');
      }, error => {
        if (error.status === 409) {
          this.dailyMenu = error.json().dto;
          // Process Daily Menu
          this.dailyMenuProcessor();
        } else if (error.status === 410) {
          if (error.json().dto !== null) {
            this.dailyMenu = error.json().dto;
            // Process Daily Menu
            this.dailyMenuProcessor();
          } else {
            // Daily Menu was deleted
            this.dailyMenu = undefined;
          }
        }
        // Updated buttonDisable
        this.buttonDisable = false;
        // Display SnackBar
        this.openErrorSnackBar(error.json().message, 'ok');
      });
    } else {
      this.openWarningSnackBar('Please select at least one product', 'ok');
    }

  }

  // API call for updating an order
  putOrder(sendEmailPrefer: boolean): void {

    if (this.total > 0) {
      // Updated buttonDisable
      this.buttonDisable = true;

      // Construct order items
      let orderItemsArray: Array<remote.OrderItem> = this.generateOrderItemsArray();

      // Construct request DTO
      let updateDailyOrder: remote.UpdateDailyOrder = {
        version: this.dailyMenu.orderVersion,
        sendMail: sendEmailPrefer,
        date: this.dailyMenu.date,
        menuVersion: this.dailyMenu.version,
        menuId: this.dailyMenu.id,
        orderItems: orderItemsArray
      };

      // Make call to ordersIdPut API
      this.hungryService.ordersIdPut(this.dailyMenu.orderId, updateDailyOrder).subscribe(version => {
        this.dailyMenu.orderVersion = version.version;
        // Update edit mode
        this.editMode = false;
        // Update form editing status
        this.dailyOrderForm.disable();
        // Updated buttonDisable
        this.buttonDisable = false;
        // Emit dailyTotal
        this.dailyTotal.emit({
          date: this.dailyMenu.date,
          total: this.total
        });
        // Display SnackBar
        this.openSuccessSnackBar('Order updated!', 'ok');
      }, error => {
        if (error.status === 409) {
          this.dailyMenu = JSON.parse(error._body);
          // Process Daily Menu
          this.dailyMenuProcessor();
          // Display SnackBar
          this.openErrorSnackBar('You changed the order earlier. Here is the new one', 'ok');
        } else if (error.status === 410) {
          this.dailyMenu = error.json().dto;
          // Process Daily Menu
          this.dailyMenuProcessor();
          // Display SnackBar
          this.openErrorSnackBar(error.json().message, 'ok');
        } else {
          // Display SnackBar
          this.openErrorSnackBar(error.json().message, 'ok');
        }
        // Updated buttonDisable
        this.buttonDisable = false;
      });
    } else {
      this.openWarningSnackBar('Please select at least one product', 'ok');
    }

  }

  // API call for canceling an order
  cancelOrder(): void {

    // Updated buttonDisable
    this.buttonDisable = true;

    // Construct request DTO
    let menuInfo: remote.MenuInfo = {
      date: this.dailyMenu.date,
      menuVersion: this.dailyMenu.version,
      menuId: this.dailyMenu.id
    };

    // Make call to ordersIdDelete API
    this.hungryService.ordersIdDelete(this.dailyMenu.orderId, menuInfo).subscribe(() => {
      // Update order mode
      this.dailyMenu.orderId = -1;
      this.dailyMenu.orderVersion = -1;
      // Process Daily Menu
      this.dailyMenuProcessor();
      // Reset foods quantity
      for (let food of this.foods.get('all').getFoodsAsArray()) {
        food.quantity = 0;
      }
      // Updated buttonDisable
      this.buttonDisable = false;
      // Display SnackBar
      this.openSuccessSnackBar('Order deleted!', 'ok');
    }, error => {
      if (error.status === 410) {
        this.dailyMenu = error.json().dto;
        // Process Daily Menu
        this.dailyMenuProcessor();
      }
      // Update buttonDisable
      this.buttonDisable = false;
      // Display SnackBar
      this.openErrorSnackBar(error.json().message, 'ok');

    });

  }

  // API call for retrieving an order and enable edit mode.
  // Returns the updated dailyMenu.
  ordersIdGet(): void {

    // Updated buttonDisable
    this.buttonDisable = true;

    this.hungryService.ordersIdGet(
      this.dailyMenu.orderId,
      this.dailyMenu.id,
      this.dailyMenu.version,
      this.dailyMenu.date
    ).subscribe(dailymenu => {
      this.dailyMenu = dailymenu;
      // Process Daily Menu
      this.dailyMenuProcessor();
      // Update edit mode
      this.editMode = true;
      // Update dailyOrderForm edit status
      this.dailyOrderForm.enable();
      // Updated buttonDisable
      this.buttonDisable = false;
    }, error => {
      if (error.status === 410) {
        this.dailyMenu = error.json().dto;
        // Process Daily Menu
        this.dailyMenuProcessor();
      }
      // Updated buttonDisable
      this.buttonDisable = false;
      // Display SnackBar
      this.openErrorSnackBar(error.json().message, 'ok');

    });

  }

  // Generates Order Items Array for the request DTO
  generateOrderItemsArray(): Array<remote.OrderItem> {

    let value: any = this.dailyOrderForm.value;
    // Populate orderItems array
    let orderItemsArray: Array<remote.OrderItem> = new Array<remote.OrderItem>();
    for (let prop in value) {
      if (value.hasOwnProperty(prop)) {
        if (value[prop] > 0) {
          let orderItem: remote.OrderItem = {
            foodId: +prop,
            quantity: value[prop]
          };
          orderItemsArray.push(orderItem);
        }
      }
    }
    return orderItemsArray;

  }

  // Change order quantity using (+) (-) buttons
  changeQuantity(foodId: Number, direction: number): void {

    let previousValue: number = this.dailyOrderForm.get('' + foodId).value;

    switch (direction) {
      case 1:
        if (previousValue < 99) {
          this.dailyOrderForm.get('' + foodId).setValue(previousValue + 1);
        } else if (previousValue === null) {
          this.dailyOrderForm.get('' + foodId).setValue(0);
        } else {
          this.dailyOrderForm.get('' + foodId).setValue(99);
        }
        this.dailyOrderForm.markAsTouched();
        break;
      case -1:
        if (previousValue > 0) {
          this.dailyOrderForm.get('' + foodId).setValue(previousValue - 1);
        } else if (previousValue === null) {
          this.dailyOrderForm.get('' + foodId).setValue(0);
        }
        this.dailyOrderForm.markAsTouched();
        break;
      default:
        break;
    }

  }

  // Discard Changes
  discardChanges() {
    // Process Daily Menu
    this.dailyMenuProcessor();
  }

  // Handles Place Order Dialog
  openPlaceOrderDialog() {

    if (this.total > 0) {
      // Set and open Place Order Dialog
      let dialogRef = this.dialog.open(PlaceOrderDialog);
      dialogRef.componentInstance.name =
        this.authenticationService.getLoggedInUser().firstName
        + " " + this.authenticationService.getLoggedInUser().lastName;
      dialogRef.componentInstance.date = this.dailyMenu.date;
      let orderedFoods: Array<remote.Food> = new Array();
      for (let orderItem of this.generateOrderItemsArray()) {
        let orderedFood = this.foods.get('all').getFood(orderItem.foodId);
        orderedFood.quantity = orderItem.quantity;
        orderedFoods.push(orderedFood);
      }
      dialogRef.componentInstance.orderedFoods = orderedFoods;

      dialogRef.afterClosed().subscribe(result => {
        if (result === 'bonAppetitEmail') {
          this.placeOrder(true);
        } else if (result === 'bonAppetit') {
          this.placeOrder(false);
        }
      });
    } else {
      this.openWarningSnackBar('Please select at least one item', 'ok');
    }

  }

  // Handles Cancel Order Dialog
  openCancelOrderDialog(): void {

    let dialogRef = this.dialog.open(CancelOrderDialog);
    dialogRef.afterClosed().subscribe(result => {
      if (result === "yes") {
        this.cancelOrder();
      }
    });

  }

  // Opens a Success SnackBar with the requested message and action
  openSuccessSnackBar(message: string, action: string): void {

    this.snackBar.open(message, action, {
      extraClasses: ['success-snack-bar'],
      duration: 5000
    });

  }

  // Opens a Warning SnackBar with the requested message and action
  openWarningSnackBar(message: string, action: string): void {

    this.snackBar.open(message, action, {
      extraClasses: ['warning-snack-bar'],
      duration: 5000
    });

  }

  // Opens an Error SnackBar with the requested message and action
  openErrorSnackBar(message: string, action: string): void {

    this.snackBar.open(message, action, {
      extraClasses: ['error-snack-bar']
    });

  }

}

// Place Order Dialog
@Component({
  templateUrl: './place-order.dialog.html',
})
export class PlaceOrderDialog {
  public name: string;
  public date: Date;
  public orderedFoods: Array<remote.Food>;
  public sendEmail: boolean = false;
  public currency: string;

  constructor(
    public dialogRef: MdDialogRef<PlaceOrderDialog>,
    public globalSettingsService: GlobalSettingsService
  ) { }

  getTotalPrice(): number {
    let total: number = 0;
    for (let orderedFood of this.orderedFoods) {
      total += orderedFood.quantity * orderedFood.price;
    }
    return total;
  }

  returnPreference() {
    if (this.sendEmail) {
      this.dialogRef.close('bonAppetitEmail');
    } else {
      this.dialogRef.close('bonAppetit');
    }
  }
}

// Cancel Order Dialog
@Component({
  templateUrl: './cancel-order.dialog.html',
})
export class CancelOrderDialog {
  constructor(
    public dialogRef: MdDialogRef<CancelOrderDialog>
  ) { }
}
