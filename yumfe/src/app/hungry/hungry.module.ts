import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './home/home.component';
import { HistoryComponent } from './history/history.component';
import { HungryRouting } from './hungry.routing';
import { HungryApi } from '../remote';
import { SharedModule } from '../shared/shared.module';
import {
  DailyOrderComponent,
  PlaceOrderDialog,
  CancelOrderDialog
} from './home/daily-order/daily-order.component';
import { DailyOrderHistoryComponent } from './history/daily-order-history/daily-order-history.component';
import { HungryNavBarComponent } from './hungry-nav-bar/hungry-nav-bar.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    HungryRouting
  ],
  declarations: [
    HomeComponent,
    HistoryComponent,
    DailyOrderComponent,
    PlaceOrderDialog,
    CancelOrderDialog,
    DailyOrderHistoryComponent,
    HungryNavBarComponent
  ],
  providers: [
    HungryApi
  ],
  entryComponents: [
    PlaceOrderDialog,
    CancelOrderDialog
  ]
})
export class HungryModule { }
