import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserApprovalComponentComponent } from './user-approval-component.component';

describe('UserApprovalComponentComponent', () => {
  let component: UserApprovalComponentComponent;
  let fixture: ComponentFixture<UserApprovalComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserApprovalComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserApprovalComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
