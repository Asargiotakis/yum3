import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {MdDialog, MdDialogRef, MdSnackBar} from '@angular/material';
import * as remote from '../../remote';
import {AuthenticationService} from '../../shared/authentication.service';

@Component({
  selector: 'app-admin-user-approval',
  templateUrl: './user-approval.component.html',
  styleUrls: ['./user-approval.component.scss']
})
export class UserApprovalComponent implements OnInit {
  // flags to disable/enable button
  @Input()
  disabled: boolean;
  public submitting = false;

  @Input() approved: boolean;
  @Input() userId: number;
  @Output() stateChanged: EventEmitter<boolean> = new EventEmitter();

  constructor( private adminService: remote.AdminApi,  private authenticationService: AuthenticationService,
                private dialog: MdDialog, private snackBar: MdSnackBar) { }

  ngOnInit() {
    // user cannot disable himself or the super user
     if (this.userId === 1 || this.userId === this.authenticationService.getLoggedInUser().id){
      this.disabled = true;
    }
  }

  public setApproval(event) {
    this.submitting = true;
    if (this.approved) {
      this.adminService.usersIdApprovePut(this.userId, false).subscribe(success => {
        this.approved = false;
        this.submitting = false;
        this.stateChanged.emit();
        this.openSuccessSnackBar('User account status changed! ');

      }, error => {
                 if (error.status === 409) {
                   this.openDialog();
                 }else{
                   this.openErrorSnackBar('An unexprected error occured, please try again later');
                 }
                 this.submitting = false;
                  event.source.checked = true;

                });
    } else {
      this.adminService.usersIdApprovePut(this.userId, true).subscribe(success => {
        this.approved = true;
        this.submitting = false;
        this.stateChanged.emit();
        this.openSuccessSnackBar('User account status changed! ');

      }, error => { this.submitting = false;
                  event.source.checked = false;
                  this.openErrorSnackBar('An unexprected error occured, please try again later');
                 });
    }
  }

   public openDialog(){
    let dialogRef = this.dialog.open(UserApprovalDialog);
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'ok') {
        this.submitting = true;
        this.adminService.usersIdApprovePut(this.userId, false, true).subscribe(success => {
        this.approved = false;
        this.submitting = false;
        this.stateChanged.emit();
        this.openSuccessSnackBar('User account status changed! ');
      },error => {
        this.submitting = false;
         this.openErrorSnackBar('An unexprected error occured, please try again later'); });
      }
    });

  }

  public openSuccessSnackBar(message: string) {
    this.snackBar.open(message, 'ok', {
      extraClasses : ['success-snack-bar'],
      duration: 5000
    });

  }

    public openErrorSnackBar(message: string) {
    this.snackBar.open(message, 'ok', {
      extraClasses : ['error-snack-bar'],
      duration: 5000
    });

  }
}

@Component({
  selector: 'app-user-approval-dialog',
  templateUrl: './user-approval.dialog.html',
})
export class UserApprovalDialog{
  constructor(public dialogRef: MdDialogRef<UserApprovalDialog>) {}
}
