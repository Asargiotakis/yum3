import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatePipe } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';

import { ChefApi } from '../remote';

import {
  FoodComponent,
  EditFoodDialog,
  ArchivedFoodDialog
} from './home/food/food.component';

import { FoodsService } from './foods.service';

import { ChefRouting } from './chef.routing';

import { HomeComponent } from './home/home.component';
import { FoodEditComponent } from './home/food-edit/food-edit.component';
import { MenusComponent } from './menus/menus.component';
import { DailyMenuComponent } from './menus/daily-menu/daily-menu.component';
import { DailyOrdersComponent } from './ordersday/daily-orders.component';
import { UserOrderComponent } from './ordersday/user-order/user-order.component';
import { FoodItemComponent } from './ordersday/food-item/food-item.component';
import { OrdersComponent } from './orders/orders.component';
import { OrderTotalComponent } from './orders/order-total/order-total.component';
import { ActiveFoodsAutoCompleteComponent } from './menus/daily-menu/active-foods-auto-complete/active-foods-auto-complete.component';
import { MdAutocompleteModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ChefRouting,
    ReactiveFormsModule,
    MdAutocompleteModule
  ],
  declarations: [
    FoodComponent,
    HomeComponent,
    FoodEditComponent,
    EditFoodDialog,
    MenusComponent,
    DailyMenuComponent,
    ArchivedFoodDialog,
    DailyOrdersComponent,
    UserOrderComponent,
    FoodItemComponent,
    OrdersComponent,
    OrderTotalComponent,
    ActiveFoodsAutoCompleteComponent
  ],
  providers: [
    ChefApi,
    FoodsService,
    DatePipe
  ],
  entryComponents: [
    EditFoodDialog,
    ArchivedFoodDialog
  ],
  bootstrap: [ActiveFoodsAutoCompleteComponent]
})
export class ChefModule { }
