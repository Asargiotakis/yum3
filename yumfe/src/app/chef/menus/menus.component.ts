import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MdSnackBar } from '@angular/material';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

import { ChefRoutingComponent } from '../shared/chef-routing/chef-routing.component';
import { FoodsService } from '../foods.service';
import * as remote from '../../remote';
import { MonthlyGridCalculator } from '../../shared/monthly-grid-calculator';

@Component({
  selector: 'app-menus',
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.scss']
})
export class MenusComponent implements OnInit {

  private monthlyGridCalculator: MonthlyGridCalculator = new MonthlyGridCalculator();

  public month: number;
  public year: number;
  public basePath: string = 'chef/menus';
  public viewMonth: Date;
  public daysInGrid: Array<Date>;
  public dailyMenusChefMap: Map<number, remote.DailyMenuChef> = new Map();
  public monthlyTotal: number = 0;
  public activeFoodsOriginal = [];
  public foods: Map<number, remote.Food> = new Map();
  public foodsMap: Map<string, Object>;
  public complete: boolean = false;

  constructor(
    private router: Router,
    private chefService: remote.ChefApi,
    private foodService: FoodsService,
    private route: ActivatedRoute,
    private datePipe: DatePipe,
    public snackBar: MdSnackBar
  ) { }


  ngOnInit() {

    this.route.params.subscribe(params => {
      // Getting all active foods for the select option
      this.foodService.getActiveFoods().subscribe(activeFoods => {
        for (let food of activeFoods) {
          this.activeFoodsOriginal.push(food);
        }
      });

      if (params['year'] === undefined && params['month'] === undefined) {
        // Undefined parameters
        this.menusMonthlyGet();
        this.month = new Date().getMonth();
        this.year = new Date().getFullYear();

      } else if (!Number(params['year']) || !Number(params['month'])
        || params['year'].length !== 4 || params['month'].length !== 2
        || params['year'] < 1900 || params['month'] < 1 || params['month'] > 12) {
        // Wrong parameters
        this.router.navigate(['chef/menus']);

      } else if (Number(params['year']) === new Date().getFullYear()
        && Number(params['month']) === (new Date().getMonth() + 1)) {
        // Parameters refer to current month and year
        this.router.navigate(['/chef/menus']);

      } else {
        // Valid parameters
        this.menusMonthlyMonthYearGet(params['month'] + '-' + params['year']);
        this.month = +params['month'] - 1;
        this.year = +params['year'];
      }

      this.viewMonth = new Date(this.year, this.month);

    });


  }//end of init

  //Creates the current's month menu
  menusMonthlyGet() {
    this.chefService.dailyMenusMonthlyGet().subscribe(menus => {
      //this.menusMonthlyGet
      this.foodService.getDailyMenuFoodsMap(menus).subscribe(foods => {
        this.foodsMap = foods;
      },
        error => { },
        () => {
          this.complete = true;
        }
      );
      this.dailymenusProcessor();
    }, error => {
      this.openErrorSnackBar('An error occurred', 'ok');
    });
  }

  //Creates the requested month menu by giving as argument the MM-YYYY from URL
  menusMonthlyMonthYearGet(weekYear: string) {

    this.chefService.dailyMenusMonthlyMonthYearGet(weekYear).subscribe(menus => {
      this.foodService.getDailyMenuFoodsMap(menus).subscribe(foods => {
        this.foodsMap = foods;
      },
        error => { },
        () => {
          this.complete = true;
        }
      );
      this.dailymenusProcessor();
    }, error => {
      this.openErrorSnackBar('An error occurred', 'ok');
    });
  }

  // Populates a Map of dailymenus and an array of the current month's days
  dailymenusProcessor(): void {

    // Populate dates of month (weekly view)
    this.daysInGrid = this.monthlyGridCalculator
      .setDaysInGrid(new Date(this.year, this.month));
  }

  getDailyOrders(day) {
    let keyDay = this.datePipe.transform(new Date(day), 'yyyy-MM-dd');
    return this.foodsMap.get(keyDay);
  }

  // Opens an Error SnackBar with the requested message and action
  openErrorSnackBar(message: string, action: string): void {

    this.snackBar.open(message, action, {
      extraClasses: ['error-snack-bar']
    });

  }

}
