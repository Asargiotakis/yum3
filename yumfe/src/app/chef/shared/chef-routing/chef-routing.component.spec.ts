import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChefRoutingComponent } from './chef-routing.component';

describe('ChefRoutingComponent', () => {
  let component: ChefRoutingComponent;
  let fixture: ComponentFixture<ChefRoutingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChefRoutingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChefRoutingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
