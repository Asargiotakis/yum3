import {
  Component,
  OnInit
} from '@angular/core';
import { MdSnackBar } from '@angular/material';
import * as remote from '../../remote';

import { PaginationComponent } from '../../shared/pagination/pagination.component';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public model: remote.FoodDetails = {};
  public checkOnNameIsCompleted = false;

  public foodTypes = [
    { value: 'main', viewValue: 'Main meal', selected: true },
    { value: 'salad', viewValue: 'Salad' },
    { value: 'drink', viewValue: 'Drink' }
  ];
  public selecedFoodType: string;

  public pageSizes = [
    { value: '10', viewValue: '10 items', selected: true },
    { value: '20', viewValue: '20 items' },
    { value: '50', viewValue: '50 items' },
    { value: '100', viewValue: '100 items' }

  ];
  public selecedPageSize: number = 10;

  public orderingVals = [
    { value: '', viewValue: 'Newest', selected: true },
    { value: 'lowestprice', viewValue: 'Lower price' },
    { value: 'highestprice', viewValue: 'Higher price' }
  ];
  public selecedOrderingValue: string;

  public showArchived: boolean = false;
  public currentPage: number = 1;
  public totalPages: number = 1;
  public foodItems: Array<remote.FoodItem>;
  public ready: boolean = false;
  public disableCreateButton: boolean = false;
  public loading: boolean = false;
  public haveFoods: boolean;

  constructor(
    private chefService: remote.ChefApi,
    public snackBar: MdSnackBar
  ) { }


  ngOnInit() {
    this.currentPage = 1;

    // get the first 10 foods from the db
    this.chefService.foodsGet().subscribe(foodItems => {
      // get the initial foodItems and store it.
      this.foodItems = foodItems.foodItems;
      // set the totalPages
      this.totalPages = !foodItems.totalPagesNumber ? 1 : foodItems.totalPagesNumber;
      this.haveFoods = this.foodItems.length > 0 ? true : false;
    },
      error => { },
      () => this.ready = true);
  }

  createFood(form) {
    this.loading = true;
    this.disableCreateButton = true;
    this.chefService.foodsPost(this.model)
      .subscribe(result => {
        this.openSuccessSnackBar("Food created successfully", "ok");
        this.refreshList({ fullRefresh: true, keepPageSize: false });
        form.reset();
      }, error => {
        if (error.status === 400) {
          this.openErrorSnackBar("Food could not be created", "ok");
          this.disableCreateButton = false;
          this.loading = false;
        } else if (error.status === 412) {
          this.openErrorSnackBar("Food name already exists", "ok");
          this.disableCreateButton = false;
          this.loading = false;
        } else {
          this.openErrorSnackBar("Internal Server Error", "ok");
          this.disableCreateButton = false;
          this.loading = false;
        }
      },
      () => {
        this.loading = false;
        this.disableCreateButton = false;
      });
  }

  refreshList(ev) {

    if (ev.fullRefresh) {

      //if we want to refresh the food list without loose the
      //current page size
      let pageSize = ev.keepPageSize ? this.selecedPageSize : null;

      this.chefService.foodsGet(null, null, null, null, null, pageSize)
        .subscribe(foodItems => {
          this.foodItems = foodItems.foodItems;
          this.totalPages = !foodItems.totalPagesNumber ? 1 : foodItems.totalPagesNumber;
          this.currentPage = 1;
          this.haveFoods = this.foodItems.length > 0 ? true : false;
        },
        error => { });

      this.resetControls();
    } else {
      this.updatePage();
    }

  }

  //handles the events that are for refreshing the page,emitted from the edit food component
  // and when we create a new food.
  updatePage() {
    this.ready = false;
    let pageSize = Number(this.selecedPageSize);

    this.chefService.foodsGet(null, this.showArchived,
      this.selecedFoodType, this.selecedOrderingValue, this.currentPage,
      pageSize || null).subscribe(foodItems => {

        this.foodItems = foodItems.foodItems;
        this.totalPages = !foodItems.totalPagesNumber ? 1 : foodItems.totalPagesNumber;
        this.haveFoods = this.foodItems.length > 0 ? true : false;

        //if we the requested page return an empty list
        if (foodItems.foodItems.length === 0) {
          //make a new request with current page equal to
          //the total pages of the previous respone.
          this.chefService.foodsGet(null, this.showArchived,
            this.selecedFoodType, this.selecedOrderingValue, this.totalPages,
            pageSize || null).subscribe(foodItems => {
              this.currentPage = !this.totalPages ? 1 : this.totalPages;
              this.foodItems = foodItems.foodItems;
              this.totalPages = !foodItems.totalPagesNumber ? 1 : foodItems.totalPagesNumber;

              this.ready = false;
            },
            error => { },
            () => this.ready = true);
        }

      }, error => { },
      () => this.ready = true);
  }

  resetControls() {
    this.showArchived = false;
    this.selecedFoodType = '';
    this.selecedOrderingValue = '';
    this.selecedPageSize = 10;
    this.currentPage = 1;
  }

  //handles the events come from pagination component
  pageChanged(page: number) {
    this.currentPage = page;
    this.updatePage();
  }

  checkIfExists(name) {
    if (name) {
      this.chefService.foodsFindByNameNameGet(name)
        .subscribe(
        foodItem => {
          if (!foodItem.archived) {
            this.disableCreateButton = true;
          }
        },
        error => {
          // if the food name exists
          if (error.status === 404) {
            this.disableCreateButton = false;
          } else {
            this.openErrorSnackBar("An error occurred", "ok");
          }
        },
        () => {
          this.checkOnNameIsCompleted = true;
        });
    }
    else {
      this.disableCreateButton = false;
    }
  }

  // Opens a Success SnackBar with the requested message and action
  openSuccessSnackBar(message: string, action: string): void {

    this.snackBar.open(message, action, {
      extraClasses: ['success-snack-bar'],
      duration: 5000
    });

  }

  // Opens a Warning SnackBar with the requested message and action
  openWarningSnackBar(message: string, action: string): void {

    this.snackBar.open(message, action, {
      extraClasses: ['warning-snack-bar'],
      duration: 5000
    });

  }

  // Opens an Error SnackBar with the requested message and action
  openErrorSnackBar(message: string, action: string): void {

    this.snackBar.open(message, action, {
      extraClasses: ['error-snack-bar']
    });

  }

}
