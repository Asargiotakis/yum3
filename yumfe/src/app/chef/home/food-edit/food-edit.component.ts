import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter
} from '@angular/core';

import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms'

import {
  MdDialog,
  MdDialogRef,
  MdSnackBar
} from '@angular/material';

import * as remote from '../../../remote';




@Component({
  selector: 'app-chef-food-edit',
  templateUrl: './food-edit.component.html',
  styleUrls: ['./food-edit.component.scss']
})
export class FoodEditComponent implements OnInit {

  public foodTypes = [
    { value: 'main', viewValue: 'Main meal' },
    { value: 'salad', viewValue: 'Salad' },
    { value: 'drink', viewValue: 'Drink' }
  ];

  public editFoodForm: FormGroup;

  @Input()
  public foodItemEditable: remote.FoodItemEditable;
  @Input()
  public clone: boolean;
  @Output()
  public foodItemEdited = new EventEmitter();
  public disableButtons: boolean = false;


  constructor(
    private chefService: remote.ChefApi,
    private dialog: MdDialog,
    public snackBar: MdSnackBar
  ) { }

  ngOnInit() {
    //init form fields with the initial vaules from the selected foodItem
    //disable the fields that can't be edited.
    this.editFoodForm = new FormGroup({
      name: new FormControl({
        value: this.foodItemEditable.foodItem.name,
        disabled: !this.foodItemEditable.editable && !this.clone,
      },
        [
          Validators.required,
          Validators.maxLength(100)
        ]
      ),
      foodType: new FormControl({
        value: this.foodItemEditable.foodItem.foodType.toLowerCase(),
        disabled: !this.foodItemEditable.editable && !this.clone
      },
        [Validators.required]),
      price: new FormControl({
        value: this.foodItemEditable.foodItem.price,
        disabled: !this.foodItemEditable.editable && !this.clone
      },
        [Validators.required, Validators.pattern('^[0-9]{1,2}(\.[0-9]{1,2})?$')]),
      description: new FormControl(this.foodItemEditable.foodItem.description,
        [Validators.maxLength(250)])
    },
    );

  }

  updateFood({ value, valid }: { value: remote.FoodDetails, valid: boolean },
    event: Event) {
    this.disableButtons = true;
    let updateFoodDetails = <remote.UpdateFoodDetails>{};
    updateFoodDetails.version = this.foodItemEditable.version;

    let foodDetails = <remote.FoodDetails>{};
    //set the the food details from form to the foods default properties
    //if  we get them from form as undefined (reason: form field was desabled).
    foodDetails.description = value.description;
    foodDetails.foodType = value.foodType || this.foodItemEditable.foodItem.foodType;
    foodDetails.name = value.name || this.foodItemEditable.foodItem.name;
    foodDetails.price = value.price || this.foodItemEditable.foodItem.price;

    updateFoodDetails.FoodDetails = foodDetails;

    this.chefService.foodsIdPut(this.foodItemEditable.foodItem.id, updateFoodDetails, this.clone)
      .subscribe(version => {
        let changedFoodItem = <remote.FoodItem>{
          id: this.foodItemEditable.foodItem.id,
          name: foodDetails.name,
          price: foodDetails.price,
          foodType: foodDetails.foodType.toUpperCase(),
          description: foodDetails.description,
          archived: this.foodItemEditable.foodItem.archived
        }
        this.foodItemEdited.emit(changedFoodItem);
      },
      error => {
        if (error.status === 404) {
          this.snackBar.open("This food item has been deleted", "ok", {
            extraClasses: ['error-snack-bar']
          });
          this.disableButtons = false;
          this.foodItemEdited.emit();
        } else if (error.status === 409) {
          this.snackBar.open("Concurrent Modification Error, this food item has been modified earlier", "ok", {
            extraClasses: ['error-snack-bar']
          });
          this.disableButtons = false;
          this.foodItemEdited.emit();
        } else {
          this.snackBar.open("Internal Server Error", "ok", {
            extraClasses: ['error-snack-bar']
          });
          this.disableButtons = false;
        }
      },
      () => this.disableButtons = false
      );
  }

  cancelEditing(): void {
    this.clone = undefined;
    this.disableButtons = false;
    this.foodItemEdited.emit();
  }

}
